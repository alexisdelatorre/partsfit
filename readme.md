# Parts Fits Autoparts

Main repository for all the tools used on Parts Fit Autopartes.

## Geting started

### Dependencies:

- [Postgres](https://www.postgresql.org/)
- [Minio](https://www.minio.io/) or any [S3](https://aws.amazon.com/s3/) compatible software

ENV variables required:
````
MELI_CLIENT_ID (Mercado Libre client id)
MELI_CLIENT_SECRET (Mercado Libre secret)

PGUSER (Postgres user)
PGPASSWORD (Postgres password)
PGDATABASE (Postgres database)

S3_ENDPOINT (S3 host)
S3_ACCESS_KEY (S3 key)
S3_SECRET (S3 secret)

PROXIES (A list of proxies separated by comma)
````

---

## Scrapers (directory: scripts/scrape)

### Aldo
http://www.aldoautopartes.com/pi_busqueda.jsp

Biggest catalog (Around 23,000 unique pieces) almost all the pieces are bodyparts. online catalog updated once a day around 11:00 am.

There is no way of knowing compatible cars for a single piece without parsing the title so there are 2 different scrapers for Aldo.

The first one (aldo-parts.js) scrapes pieces by category and gets all informaton except compatible cars. this scraper is relatiblaly fast.

The second one (aldo-cars.js) scrapes every model of every year even if there are no parts for that year. The process is long and almost never changes information.

### Optimo
http://www.optimoautopartes.mx/productos.php

Small catalog (Around 3000 unique pieces) most body parts sourced from Aldos, the rest are mechanical parts.
Some logistic problems and the online catalog is not up to date.
A manual inventory is done every two weeks and we receive a updated catalog by email.

### Radec: 
http://www.radec.com.mx/catalogo

Discontinued. Really bad logistic and shipping times

---

## Mercado Libre (directory: scripts/meli)

- get_items
- sell_items
- syncronise_items

---

## Amazon Mexico (directory: scripts/amazon)

Work in progress...

---

## Store
source code for the store located at http://www.partsfitauto.com

### How to start the store locally

1. Open two terminal windows, one for the server and one for the client.
2. On the first terminal execute `cd store/server && npm install && node .`
3. On the second terminal execute `cd store/server && npm install && npm run start`
4. Go to http://localhost:3000

---

## Utils

### price.js

Tool for calculating post prices on Mercado Libre. Only for mexico for now, make a pr if you want to addapt it to your country

Usage:
````javascript
const price = require('meli-price')
price(args) // gets you a float with the recommended price
````

Arguments:
  - price :: The original vendor price of the item
  - discount :: Vendor discount on the item
  - tax :: Your country tax applied to the vendor price
  - shipping :: Mercado Libre's or your own shipping price
  - commision :: Mercado Libre's commision (13% for standard post & 17.5% for premium post)
  - less_than_550 :: Mercado LIbre's discount on shipping on items under 550 mxn
  - more_than_550 :: Mercado LIbre's discount on shipping on items over 550 mxn
  - expectedProfit :: expected profit after all costs, comissions and shipping are paid

### meli/description.js
### meli/title.js
### meli/missing.js
### meli/parse_description.js
### amazon/description.js

---

## Daemons

### sync_db.js

Uses mercadolibre webhook to update the db everytime an item changes status or there is a sale.

To use on development enviroment:
`ssh -R 80:localhost:8888 root@138.68.40.29`
(you need ssh access to the server)




