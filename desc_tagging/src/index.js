import 'antd/dist/antd.css'

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { get } from 'axios'

import { Button } from 'antd'
import { Divider } from 'antd'
import { Input } from 'antd'
import { Popover } from 'antd'

const Wrapper = styled.div`
  display: flex;
  margin-top: 5em;
  margin-right: auto;
  margin-left: auto;
`

const Examples = styled.div`
  width: 500px;
`

const Controls = styled.div`
  margin-left: 4em;
  width: 500px;
`

class Editor extends Component {
  state = {
    original: 'aaa',
    clean: 'bbb',
  }

  render () {
    return (
      <div>
        Original: <Input onChange={ev => this.setState({ original: ev.target.value })}/><br/>
        Limpio: <Input onChange={ev => this.setState({ clean: ev.target.value })}/><br/>
        <Button onClick={() => {
          this.props.onSave(this.state)
          this.props.vis()
        }}>Guardar</Button>
      </div>
    )
  }
}

class App extends Component {
  state = {
    desc: [],
    cats: [
      { original: 'BRASILENO', clean: 'brasil' },
      { original: 'S/ARNES', clean: 'sin arnes' },
      { original: 'TYC', clean: 'marca TYC' },
      { original: 'IZQ', clean: 'lado derecho' },
      { original: 'DER', clean: 'lado izquierdo' },
    ],
    visible: false,
  }

  constructor(props) {
    super(props)

    get('http://localhost:8000')
      .then(x => x.data)
      .then(desc => this.setState({ desc }))
  }

  change_desc = () => {
    get('http://localhost:8000')
      .then(x => x.data)
      .then(desc => this.setState({ desc }))
  }

  add_cat = ({ original, clean }) => {
    this.setState({ cats: this.state.cats.concat([{ original, clean }]) })
  }

  hide = () => {
    this.setState({ visible: false })
  }

  handleVisibleChange = (visible) => {
    this.setState({ visible });
  }

  render() {
    return (
      <Wrapper>
        <Examples>
          {this.state.desc.map((desc, i) => (
            <div key={i}>
              {i !== 0 && <Divider/>}
              <div>{desc}</div>
            </div>
          ))}
          <Divider/>
          <Button onClick={this.change_desc}>Nueva Descripcion</Button>
        </Examples>
        <Controls>
          <div style={{height: 320, overflow: 'auto'}}>
            {this.state.cats.map(cat => (
              <div>
                [{cat.original}] [{cat.clean}]
                <Button>Editar</Button>
              </div>
            ))}
          </div>
          <div>
            <Popover content={<Editor onSave={this.add_cat} vis={this.hide}/>} trigger="click" visible={this.state.visible} onVisibleChange={this.handleVisibleChange}>
              <Button>Nueva Categoria</Button>
            </Popover>
          </div>
        </Controls>
      </Wrapper>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
