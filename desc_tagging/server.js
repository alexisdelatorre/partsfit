const express = require('express')
const app = express()
const cors = require('cors')

const { Client: _pg } = require('pg')

let pg
(async () => {
  pg = new _pg()
  await pg.connect()
})()

const get_desc = async () => {
  const query = `
    select * from parts
    limit 5
    offset 1130
  `
  return await pg.query(query)
    .then(x => x.rows.map(x => x.title))
}

app.use(cors())

app.get('/', (req, res) => {
  get_desc()
    .then(x => res.send(x))
})

app.listen(8000, () => console.log('Example app listening on port 8000!'))