const http = require('http')
const { Client: _pg } = require('pg')
const { get } = require('axios')

const host = 'https://api.mercadolibre.com'

const pg = new _pg()
pg.connect() // async

const update_post = async post_id => {

  let post
  post = await get(`${host}/items/${post_id}`)
  post = post.data

  const id = post.id
  const title = post.title
  const category = post.category_id
  const inventory = post.available_quantity
  const price = post.price
  const premium = post.listing_type_id === 'gold_pro'
  const status = post.status

  const values = [
    id,
    title,
    category,
    inventory,
    price,
    premium,
    status,
    post_id,
  ]

  const query = `
    update posts 
    set 
      id = $1,
      title = $2,
      category = $3,
      inventory = $4,
      price = $5,
      premium = $6,
      status = $7
    where id = $8
  `
  
  await pg.query(query, values)

  console.log(`update: ${post_id}`)
  console.log(values)
  console.log()
  
} 

const server = http.createServer((req, res) => {
  if (req.method === 'POST') {
    let body = ''
    req.on('data', chunk => {
        body += chunk.toString()
    })
    req.on('end', () => {
        body = JSON.parse(body)
        if (body.topic === 'items') {
          const id = body.resource.replace('/items/', '')
          update_post(id)
        }
        res.end('ok')
    })
  }
})

server.listen(8888, () => console.log('listening on port 8888'))