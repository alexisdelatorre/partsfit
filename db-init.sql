create database partsfit;

create table parts (
    id text not null,
    vendor_id text not null,
    title text not null,
    inventory int not null,
    price numeric not null,
    category text not null,
    image text not null,
    link text,
    codes text,
    manufacturer text,
    side text
);

create table cars (
    part_id text not null,
    vendor_id text not null,
    make text not null,
    model text not null,
    year_init int not null,
    year_last int not null,
    unique (part_id, vendor_id, make, model)
);

create table posts_meli (
    id text primary key,
    title text not null,
    category text not null,
    inventory int not null,
    price int not null,
    isPremium boolean not null,
    status text not null,
    description text not null,
    account_id text not null,
    car_id text references cars(id)
);

create table posts_amz (
    asin text primary key,
    part_id text not null,
    inventory int not null,
    price numeric not null
);

-- DOWN
-- drop table posts_amz;
-- drop table posts_meli;
-- drop table cars;
-- drop table parts;

