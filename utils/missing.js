const R = require('ramda')
const includes = require('lodash.includes')

const missing = (cars, posts) => {
  const hash = x =>
  `${x.make.toLowerCase()}-${x.model.toLowerCase()}-${x.premium}`
  // `${x.make.toLowerCase()}-${x.model.toLowerCase()}-${x.yearinit || x.year_init}-${x.yearlast || x.year_last}-${x.premium}`

  const hashedCars =
  R.chain(car => [
    { premium: false, ...car },
    { premium: true, ...car },
  ], cars)
  .map(car => ({ ...car, hash: hash(car) }))

  return (
    R.reject(car => includes(posts.map(hash), car.hash), hashedCars)
    .map(R.dissoc('hash'))
  )
}

module.exports = missing
