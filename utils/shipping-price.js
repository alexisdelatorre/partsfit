const { get } =  require('axios')

const host = 'https://api.mercadolibre.com'

const shipping_price = async category_id => {
  let dimensions
  dimensions = await get(`${host}/categories/${category_id}/shipping`)
  dimensions = dimensions.data

  const { height, width, length, weight } = dimensions

  const link = [
    host,
    '/sites',
    '/MLM',
    '/shipping_options',
    '/free',
    '?dimensions=',
    `${height}x${width}x${length},${weight}`,
  ].join('')

  let shipping_price
  shipping_price = await get(link)
  shipping_price = shipping_price.data
  shipping_price = shipping_price.coverage.all_country.list_cost

  return shipping_price
}

module.exports = shipping_price