const auth = require('meli-auth')
const get = require('axios').get

const shippingMe = {
    mode: 'me2',
    local_pick_up: false,
    free_shipping: true,
    free_methods: [{ id: 501245, rule: { free_mode: 'country', value: null } }],
    dimensions: null,
    tags: ['me2_available'],
    logistic_type: 'drop_off',
    store_pick_up: false
  }
  
const shipping = {
    "mode": "not_specified",
    "dimensions": null,
    "local_pick_up": false,
    "free_shipping": true,
    "logistic_type": "not_specified",
    "store_pick_up": false
}

function Client (args) {
    // if (args.token) this.token = args.token
    // else throw new Error('should initialize with a meli token')

    this.host = 'https://api.mercadolibre.com'
}

Client.prototype.authenticate = async function () {
    this.token = await auth({}).promise()
    this.user = await this.getUser()
}

Client.prototype.post = async function (args) {
    const link = `${this.host}/items?access_token=${this.token}`

    const body = {
        title: args.title,
        description: { plain_text: args.description },
        price: args.price,
        available_quantity: args.inventory,
        category_id: args.category,
        pictures: args.images.map(source => ({ source })),
        listing_type_id: args.isPremium ? 'gold_pro': 'gold_special',
        shipping: args.usesMe ? shippingMe : shipping,
        currency_id: 'MXN',
        condition: 'new',
    }

    return post(link, body).then(x => x.data.id)
}

// Client.prototype.update = async function (args) {
//     const body = {}

//     if (args.price) {
//         body.price = args.price
//     }

//     console.log('update:', body)
// }

Client.prototype.getUser = async function () {
    if (!this.user) {
        const link = `${this.host}/users/me?access_token=${this.token}`
        return get(link).then(x => x.data)
    }
    else {
        return this.user
    }
}

Client.prototype.getItem = async function (postId) {
    const itemLink = `${this.host}/items/${postId}`
    const descLink = `${this.host}/items/${postId}/description`

    const post = await get(itemLink)
        .then(x => x.data)

    const id = post.id
    const title = post.title
    const category = post.category_id
    const inventory = post.initial_quantity
    const price = post.price
    const premium = post.listing_type_id === 'gold_pro'
    const status = post.status

    const description = await get(descLink)
        .then(x => x.data.plain_text)

    return {
        id,
        title,
        category,
        inventory,
        price,
        premium,
        status,
        description
    }
}

Client.prototype.getSellerItems = async function (_scroll) {
    let link
    link = [
        this.host,
        '/users',
        `/${this.user.id}`,
        '/items',
        '/search',
        '?search_type=scan',
        `&access_token=${this.token}`,
    ].join('')
    if (_scroll) link += `&scroll_id=${_scroll}`

    const search = await get(link)
        .then(x => x.data)

    const items = search.results
    const scroll = search.scroll_id
    const total = search.paging.total

    return { items, scroll, total }
}

Client.prototype.updateStatus = async function (id, status) {
    const link = `${this.host}/items/${id}?access_token=${this.token}`
    await put(link, { status })
}

Client.prototype.delete = async function (id) {
    const link = `${this.host}/items/${id}?access_token=${this.token}`
    await put(link, { deleted: true })
}

async function predictCat (title) {
    const link = `${this.host}/sites/MLM/category_predictor/predict?title=${qs({title})}`
    return get(link).then(x => x.data.id)
}

module.exports = { Client, predictCat }