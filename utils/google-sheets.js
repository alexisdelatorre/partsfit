const _gsheet = require('google-spreadsheet')


const create_gsheet = async (sheet_id, creds) => {
  const doc = new _gsheet(sheet_id)

  this.add_row = (worksheet_id, new_row) => new Promise((resolve, reject) => {
    doc.addRow(worksheet_id, new_row, (err, row) => {
      if (err) reject(err)
      else resolve(row)
    })
  })

  this.get_info = () => new Promise((resolve, reject) => {
    doc.getInfo((err, info) => {
      if (err) reject(err)
      else resolve(info)
    })
  })

  this.get_rows = (worksheet_id, _opts) => new Promise((resolve, reject) => {
    let opts
    if (_opts) opts = _opts
    else opts = {}

    doc.getRows(worksheet_id, opts, (err, rows) => {
      if (err) reject(err)
      else resolve(rows)
    })
  })

  await new Promise((resolve, reject) => {
    doc.useServiceAccountAuth(creds, () => {
      resolve(doc)
    })
  })

  return this
}

module.exports = create_gsheet