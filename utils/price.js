const price = input => {
  const { price } = input
  const { shipping } = input
  const { expectedProfit } = input
  const { commision } = input
  const { discount } = input
  const { tax } = input
  const { less_than_550 } = input
  const { more_than_550 } = input
  const { me } = input

  const en_1 = me ? shipping * (1 - less_than_550) : shipping
  const en_2 = me ? shipping * (1 - more_than_550) : shipping

  // Precio de Compra = ( Precio Anunciado - Descuento de Proveedor ) + IVA
  const pc = price * (1 - discount) * (1 + tax)

  // Precio de Venta = ( Precio de Compra * Margen de Ganancia Deseado + Envio ) / Comision
  const pv = (pc * (1 + expectedProfit) + shipping) / (1 - commision)

  const pv_1 = (pc * (1 + expectedProfit) + en_1) / (1 - commision)
  const pv_2 = (pc * (1 + expectedProfit) + en_2) / (1 - commision)

  const pv_aj = pv_1 < 550 ? pv_1 : pv_2

  // Comision de Mercado Libre
  const ml = pv_aj * commision

  // Ganancia Bruta = Precio de Venta - Precio de compra
  const gb = pv_aj - pc

  // Ganancia Neta = Ganancia Bruta - Comision de Mercado Libre
  const gn = gb - ml - (pv_aj < 550 ? en_1 : en_2)

  // Porcentaje de Ganancia
  const pg = gn / pc

  return parseFloat(pv_aj.toFixed(2))
}

// function price (input) {
//   const vendorPrice = input.vendorPrice
//   const vendorDiscount = input.vendorDiscount
//   const tax = input.tax
//   const shipping = input.shipping
//   const expectedProfit = input.expectedProfit
//   // this is the number that the shipping will be multiplied by when
//   // sale price is less than 550.
//   const shippingDiscount = input.shippingDiscount
//   // will it use Mercado Envios or generate a shipping guide? (boolean)
//   const me = input.me
//   const isPremium = input.isPremium

//   const meliClasicCut = 0.13
//   const meliPremiumCut = 0.175

//   let discountedShipping
//   if (me) discountedShipping = shipping * (1 - shippingDiscount)
//   else discountedShipping = shipping

//   // Precio de Compra = ( Precio Anunciado - Descuento de Proveedor ) + IVA
//   const pc = vendorPrice * (1 - vendorDiscount) * (1 + tax)

//   //  // Precio de Venta + Envio = ( Precio de Compra * Margen de Ganancia Deseado + Envio ) / Comision
 
//   let pv

//   const pvClasicPlusShipp = (pc * (1 + expectedProfit) + discountedShipping) 
//     / (1 - meliClasicCut)
//   const pvPremiumPlusShipp = (pc * (1 + expectedProfit) + discountedShipping) 
//     / (1 - meliPremiumCut)
//   const pvClasicNoShipp = pc * (1 + expectedProfit) / (1 - meliClasicCut)
//   const pvPremiumNoShipp = pc * (1 + expectedProfit) / (1 - meliPremiumCut)

//   if (!isPremium && pvClasicPlusShipp < 550) pv = pvClasicPlusShipp
//   if (!isPremium && pvClasicNoShipp > 449) pv = pvClasicNoShipp
//   else pv = pvClasicPlusShipp

//   if (isPremium && pvPremiumPlusShipp < 550) pv = pvPremiumPlusShipp
//   if (isPremium 
//     && pvPremiumNoShipp > 549 
//     && pvPremiumNoShipp < 1000) pv = pvPremiumNoShipp
//   if (isPremium && pvClasicNoShipp > 999 && me) pv = pvClasicNoShipp
//   if (isPremium && pvClasicNoShipp > 999 && !me) pv = pvPremiumNoShipp

//    return Number(pv.toFixed(2))
// }

module.exports = price
