const { encaseP2, Future } = require('fluture')
const { put } = require('axios')
const { Right, Left } = require('crocks/Either')
const createQuery = require('knex')({ client: 'pg' })
const { q: executeQuery } = require('postgres-future')
const R = require('ramda')

const inventory = (db, token, id, inventory) => {
  const host = 'https://api.mercadolibre.com'
  const link = `${host}/items/${id}?access_token=${token}`

  const meli =
  encaseP2(put, link, { available_quantity: inventory })
  .fold(() => Left({ id: id, message: 'update error' }), Right)

  const query =
  createQuery('posts')
  .where({ id })
  .update({ inventory })
  .toString()

  const database =
  executeQuery(db, query)
  .map(R.always(Right('success')))

  return meli.chain(e => e.either(
    err => Future.of(Left(err)),
    () => database
  ))
}

const price = (db, token, id, price) => {
  const host = 'https://api.mercadolibre.com'
  const link = `${host}/items/${id}?access_token=${token}`

  const meli =
  encaseP2(put, link, { price })
  .fold(() => Left({ id: id, message: 'update error' }), Right)

  const query =
  createQuery('posts')
  .where({ id })
  .update({ price })
  .toString()

  const database =
  executeQuery(db, query)
  .map(R.always(Right('success')))

  return meli.chain(e => e.either(
    err => Future.of(Left(err)),
    () => database
  ))
}

const priceAndInventory = (db, token, id, price, inventory) => {
  const host = 'https://api.mercadolibre.com'
  const link = `${host}/items/${id}?access_token=${token}`

  const meli =
  encaseP2(put, link, { price, available_quantity: inventory })
  .fold(() => Left({ id: id, message: 'update error' }), Right)

  const query =
  createQuery('posts')
  .where({ id })
  .update({ price, inventory })
  .toString()

  const database =
  executeQuery(db, query)
  .map(R.always(Right('success')))

  return meli.chain(e => e.either(
    err => Future.of(Left(err)),
    () => database
  ))
}

module.exports = { price, inventory, priceAndInventory }
