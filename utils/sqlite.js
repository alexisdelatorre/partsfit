const _sqlite = require('sqlite3')
const { Future, encaseN2 } = require('fluture')

const sqlite = {
  connect: path => {
    const db = new _sqlite.Database(path)
    return Future.of(db)
  },
  all: (client, table) => {
    const all = client.all.bind(client)
    const query = `SELECT * FROM ${table}`
    return encaseN2(all, query, [])
  },
  run: (client, query, args) => {
    const run = client.all.bind(client)
    return encaseN2(run, query, args || [])
  }
}

module.exports = sqlite
