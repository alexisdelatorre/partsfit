const log = (id, _message) => {
  count += 1

  const percent =
  x => (x*100).toFixed(2)

  const message = [
    `[${count}/${total}]`,
    `[${percent(count/total)}%]`,
    `[${id}]`,
    `${_message}\n`,
  ]

  console.log(message.join(' '))
}