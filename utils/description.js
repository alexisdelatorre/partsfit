const renderCar =
  ({ make, model, yearinit, yearlast }) =>
  `- ${make} ${model} ${yearinit}-${yearlast}`

const render =
  ({ business, make, model, category, yearinit, yearlast, side, id, title, cars, vendor_id, random }) =>
  `
  =============================

  ${business}

  =============================

  Categoria: ${category}
  Marca: ${make}
  Modelo: ${model}
  Años: ${yearinit}-${yearlast}
  Lado: ${side === 'pair' ? 'Ambos' : side === 'right' ? 'Derecho' : ( side === 'left' ? 'Izquierdo' : 'NA')}
  Numero De Parte: ${id}
  Descripcion: ${title}
  IDPR: ${vendor_id}

  =============================

  Autos Compatibles:

  ${cars.map(renderCar).join('\n')}

  =============================

  Preguntas Frecuentes:

  -
  ¿Tienes el producto en Stock?
  Si lo tenemos, todas nuestras publicaciones se retiran cuando se agota el producto.

  -
  ¿Son partes Originales?
  Todas nuestros Productos son Genéricos y de Excelente Calidad.

  -
  ¿Emiten factura?
  Si facturamos, ademas todos nuestros productos incluyen IVA.

  -
  ¿Cual es el Costo de Envio?
  Enviamos Gratuitamente a todo Mexico directamente a su domicilio.

  -
  ¿Cuando sale mi pedido?
  Una vez procesado el pago, nuestros productos se envían en las próximas 24 horas de Lunes a Viernes.

  -
  Ya salió mi pedido ¿Cuanto tarda en llegar?
  Nuestros Envíos tardan de 2 a 4 días hábiles en llegar.

  =============================
  ${random}
  `

  module.exports = render
