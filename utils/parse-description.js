const { Just, Nothing } = require('crocks/Maybe')
const R = require('ramda')

const parseDescription = description => {
  const part_id = R.pipe(
    x => x.match(/Numero De Parte: .*/g),
    x => x ? Just(x) : Nothing(),
    R.map(R.prop(0)),
    R.map(R.replace('Numero De Parte: ', '')),
    m => m.option(undefined)
  )

  const make = R.pipe(
    x => x.match(/Marca: .*/g),
    x => x ? Just(x) : Nothing(),
    R.map(R.prop(0)),
    R.map(R.replace('Marca: ', '')),
    m => m.option(undefined)
  )

  const model = R.pipe(
    x => x.match(/Modelo: .*/g),
    x => x ? Just(x) : Nothing(),
    R.map(R.prop(0)),
    R.map(R.replace('Modelo: ', '')),
    m => m.option(undefined)
  )

  const years = R.pipe(
    x => x.match(/Años: .*/g),
    x => x ? Just(x) : Nothing(),
    R.map(R.prop(0)),
    R.map(R.replace('Años: ', '')),
    m => m.option(undefined)
  )

  const vendor_id = R.pipe(
    x => x.match(/IDPR: .*/g),
    x => x ? Just(x) : Nothing(),
    R.map(R.prop(0)),
    R.map(R.replace('IDPR: ', '')),
    m => m.option(undefined)
  )

  return {
    part_id: part_id(description),
    make: make(description),
    model: model(description),
    years: years(description),
    vendor_id: vendor_id(description),
  }
}

module.exports = parseDescription
