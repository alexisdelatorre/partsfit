const R = require('ramda')

const titleSide = side => {
  if (side === 'na') return [' ', ' ']
  else if (side === 'right') return ['der','derecho']
  else if (side === 'left') return ['izq','izquierdo']
}

const title = R.pipe(
  R.evolve({ yearinit: parseInt, yearlast: parseInt }),
  R.evolve({ side: titleSide }),
  ({ category, side, make, model, yearinit, yearlast, pair }) => [
    `${pair ? 'par' : ' '} ${category} ${side[1]} ${make} ${model} ${R.join(' ', R.range(yearinit, yearlast + 1))}`,
    `${pair ? 'par' : ' '} ${category} ${side[0]} ${make} ${model} ${R.join(' ', R.range(yearinit, yearlast + 1))}`,
    `${pair ? 'par' : ' '} ${category} ${side[1]} ${make} ${model} ${R.join('-', [yearinit, yearlast])}`,
    `${pair ? 'par' : ' '} ${category} ${side[0]} ${make} ${model} ${R.join('-', [yearinit, yearlast])}`,
    `${pair ? 'par' : ' '} ${category} ${side[0]} ${model} ${R.join('-', [yearinit, yearlast])}`,
  ],
  R.map(R.replace(/  /g, '')),
  R.map(R.toLower),
  R.filter(x => x.length < 61),
  R.head
)

module.exports = title
