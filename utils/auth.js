const open_link = require('opn')
const oauth = require('simple-oauth2')
const express = require('express')
const { post } = require('axios')
const { writeFileSync: write_file } = require('fs')

const host = 'https://auth.mercadolibre.com.mx'
const link = `${host}/authorization?response_type=code&client_id=${process.env.MELI_CLIENT_ID}`;

(async () => {
  await open_link(link)
  const code = await new Promise((resolve, reject) => {
    const app = express()
    const server = app.listen(8987)
    app.get('/', (request, response) => {
      response.send('Authenticated, you can close this window now')
      server.close()
      resolve(request.query.code)
    })
  })
  const token_link = `${host}/oauth/token?grant_type=authorization_code&client_id=${process.env.MELI_CLIENT_ID}&client_secret=${process.env.MELI_CLIENT_SECRET}&code=${code}&redirect_uri=http://localhost:8987`
  const token = await post(token_link).then(x => x.data)
  console.log(token_link)
})()