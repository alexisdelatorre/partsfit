import 'antd/dist/antd.css'

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { get } from 'axios'
import map from 'lodash.map'
import styled, { injectGlobal } from 'styled-components'

import { Card } from 'antd'
import { Select } from 'antd'
import { Spin } from 'antd'

const host = 'http://192.168.0.4:8001'

const Wrapper = styled.div`
  display: flex;
`

const _Part = ({ src }) => (
  <div>
    Informacion General:
    <ul>
      {/* <li><img src={src.part.image} style={{width: '100px'}}/></li> */}
      <li>ID: {src.part.id}</li>
      <li>Descripcion: {src.part.title}</li>
      <li>Stock: {src.part.inventory}</li>
      <li>Precio: {src.part.price}</li>
      <li>Lado: {src.part.side}</li>      
      <li>Enlace: <a href={src.part.link}>{src.part.link}</a></li>
    </ul>
    Autos Compatibles:
    {src.cars.map(car => (
      <ul>
        <li>Marca: {car.make}</li>      
        <li>Modelo: {car.model}</li>      
        <li>Anos: {car.yearinit}-{car.yearinit}</li>
      </ul>
    ))}
    Publicaciones:
    {src.posts.map(post => (
      <ul>
        <li>ID: {post.id}</li>
        <li>Titulo: {post.title}</li>
        <li>{post.premium ? 'Premium' : 'Standard'}</li>
        <li>Status: {post.status}</li>
        <li>Stock: {post.inventory}</li>
        <li>Precio: {post.price}</li>
        <li>Marca: {post.make}</li>      
        <li>Modelo: {post.model}</li>
        <li>Anos: {post.yearinit}-{post.yearinit}</li>
      </ul>
    ))}
    Analisis:
    <ul>
      {map(src.parity, (v, k) => {
        if (k === 'unknown' && v.length > 0) {
          return <li>[U: {v.length}]</li>
        }
        if (k === 'unknown' && v.length === 0) {
          return
        }
        else {
          return <li>{k} [P: {v.p.length}] [S: {v.s.length}]</li>
        }
      })}
    </ul>
  </div>
)

const Part = styled(_Part)`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 1em;
`

const PartThumb = ({ src }) => (
  <Card key={src.part.id}>
    {src.part.id}<br/>
    {src.part.title}<br/>
    {src.posts.length} Publicaciones<br/>
    {map(src.parity, (v, k) => {
      if (k === 'unknown' && v.length > 0) {
        return <div>[U: {v.length}]<br/></div>
      }
      if (k === 'unknown' && v.length === 0) {
        return
      }
      else {
        return <div>{k} [P: {v.p.length}] [S: {v.s.length}]<br/></div>
      }
    })}
    <a href={`/part/${src.part.id}`}>Detalles...</a>
  </Card>
)

class Index extends Component {
  state = {
    categories: [],
    category: '',
    parts: [],
    unknown_posts: [],
    extra_posts: [],
    missing_posts: [],
    loading: false,
  }

  constructor(props) {
    super(props)

    get(`${host}/categories`)
      .then(x => x.data)
      .then(categories => this.setState({ categories }))
  }

  cat_change = cat => {
    console.log('clicked category:', cat)

    this.setState({ loading: true })

    get(`${host}/parity?category=${cat}`)
      .then(x => x.data)
      .then(x => {
        const { parts } = x
        const { extra_posts } = x
        const { missing_posts } = x
        const { unknown_posts } = x

        console.log(parts)

        this.setState({
          category: cat,
          parts,
          extra_posts, 
          missing_posts, 
          unknown_posts,
          loading: false,
        })
      })
  }

  render(){
    const categories = this.state.categories
    const missing = this.state.missing_posts
    const parts = this.state.parts

    return (
      <div>
        Categoria:
        <Select style={{minWidth: 220}} onChange={this.cat_change}>
          {categories.map(c => <Select.Option value={c}>{c}</Select.Option>)}
        </Select>
        {/* <div>{parts.map(p => <Part src={p}/>)}</div> */}
        <div>
          {!this.state.loading ? (
            parts.map(p => <PartThumb src={p}/>)
          ) : (
            <div>Cargando... <Spin/></div>
          )}
        </div>
      </div>
    )
  }
}

ReactDOM.render(<Index/>, document.getElementById('root'))