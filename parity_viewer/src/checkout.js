import 'antd/dist/antd.css'
import 'react-credit-cards/es/styles-compiled.css'

import React, { Component } from 'react'
import styled from 'styled-components'
import { get } from 'axios'

import { Modal } from 'antd'
import { Input } from 'antd'
import { InputNumber } from 'antd'
import { Divider } from 'antd'
import { Alert } from 'antd'
import { Button } from 'antd'
import { Icon } from 'antd'
import Card from 'react-credit-cards'

const Conekta = window.Conekta

Conekta.setPublicKey('key_KJysdbf6PotS2ut2')

const host = 'http://192.168.0.13:8000'

const Hdiv = styled.div`
  margin-bottom: 15px
`

class Checkout extends Component {
  // state = {
  //   error_msg: '',
  //   name: '',
  //   card_number: '',
  //   card_name: '',
  //   exp_year: '',
  //   exp_month: '',
  //   exp: '',
  //   cvc: '',
  //   calle: '',
  //   num: '',
  //   colonia: '',
  //   cp: '',
  //   ciudad: '',
  //   estado: '',
  //   referencia: '',
  //   recibe: '',
  //   sending: false,
  //   focused: "name",
  //   quantity: 1,
  // }

  state = {
    card_error_msg: '',
    form_error_msg: '',
    sending: false,
    confirm: false,
    focus: "name",
    quantity: 1,
    payment_info: {},
    name: 'Alexis De La Torre',
    phone: '6462274795',
    email: 'contacto@alexisdelatorre.com',
    card_number: '4000000000000002',
    card_name: 'Alexis De La Torre',
    exp_month: '05',
    exp_year: '2025',
    exp: '123',
    cvc: '123',
    calle: 'Bernardo Reyes',
    num: '124',
    colonia: 'Benito Juarez',
    cp: '2920',
    ciudad: 'Mexicali',
    estado: 'Baja California',
    referencia: 'Casa Azul',
    recibe: 'Mario Perez',
  }

  // constructor(props) {
  //   super(props)

  //   const success = token => {
  //     get(`${host}/pay?token=${token.id}`).then(console.log)
  //   }

  //   Conekta.setPublicKey('key_KJysdbf6PotS2ut2')

  //   const token_params = {
  //     name: 'Alexis De La Torre',
  //     number: 4000000000000002,
  //     exp_year: 2020,
  //     exp_month: 10,
  //     cvc: 123,
  //   }

  //   Conekta.Token.create({ card: token_params }, success, console.log)
  // }

  cancel_buy = () => {
    this.props.onCancel()
    this.setState({ quantity: 1 })
  }

  set_error = res => {
    console.log(res)
    const card_error_msg = res.message_to_purchaser
    this.setState({ card_error_msg, sending: false })
  }

  success = token => {
    const link = [
      host,
      '/pay',
      `?token=${token.id}`,
      `&name=${this.state.name}`,
      `&phone=${this.state.phone}`,
      `&email=${this.state.email}`,
      `&part_id=${this.props.part.id}`,
      `&quantity=${this.state.quantity}`,
      `&calle=${this.state.calle}`,
      `&num=${this.state.num}`,
      `&colonia=${this.state.colonia}`,
      `&cp=${this.state.cp}`,
      `&ciudad=${this.state.ciudad}`,
      `&estado=${this.state.estado}`,
      `&referencia=${this.state.referencia}`,
      `&recibe=${this.state.recibe}`,
    ].join('')

    get(link)
      .then(x => x.data)
      .then(order => {
        const state = { sending: false, confirm: true, payment_info: order }
        this.setState(state)
        console.log(order)
      })
  }

  send = () => {
    this.setState({ sending: true, card_error_msg: '', form_error_msg: '' })

    let missing
    missing = [
      [this.state.name, 'Nombre'],
      [this.state.phone, 'Telefono'],
      [this.state.email, 'Correo'],
      [this.state.calle, 'Calle'],
      [this.state.num, 'Numero'],
      [this.state.colonia, 'Colonia'],
      [this.state.cp, 'Codigo Postal'],
      [this.state.ciudad, 'Ciudad'],
      [this.state.estado, 'Estado'],
      [this.state.referencia, 'Referencia'],
      [this.state.recibe, 'Recibe'],
    ]
    missing = missing.filter(x => x[0] === '')
    missing = missing.map(x => x[1])

    if (missing.length === 1) {
      const form_error_msg = `El campo "${missing[0]}" es obligatorio`
      this.setState({ form_error_msg, sending: false })
      return
    }
    else if (missing.length > 1) {
      const form_error_msg = `Los siguientes campos son obligatorios: [${missing.join(', ')}]`
      this.setState({ form_error_msg, sending: false })
      return
    }

    const token_params = {
      number: this.state.card_number,
      name: this.state.card_name,
      exp_year: this.state.exp_year,
      exp_month: this.state.exp_month,
      cvc: this.state.cvc,
    }

    Conekta.Token.create({ card: token_params }, this.success, this.set_error)
  }

  card_disclaimer = <span>No guardamos informacion sobre tu tarjeta, realizamos cobros atraves del servicio de <a href="https://www.conekta.com/"><strong>Conekta</strong></a>.</span>
  
  contact_disclaimer = 'Esta informacion es necesaria para que el equipo de Parts Fit y/o de paqueteria se contacte en caso de cualquier problema.'
  
  render(){
    const sending = this.state.sending

    return (
      <Modal
        visible={this.props.visible}
        maskClosable={false}
        footer={(
          <div>
            {!this.state.confirm &&
              <Button onClick={this.cancel_buy}>
              Cancelar
              </Button>
            }
            <Button
               type="primary" 
               loading={sending}
               onClick={() => this.state.confirm ? this.cancel_buy() : this.send() }
            >
              {this.state.confirm ? 'Ok' : 'Comprar'}
            </Button>
          </div>
        )}
      >
        {!this.state.confirm ? 
          <div>
            <Divider>Informacion de la Compra</Divider>
            <strong>id: </strong>{this.props.part.id}<br/>
            <strong>Descripcion: </strong>{this.props.part.title}<br/>
            <strong>Precio Unitario: </strong>${this.props.part.price.toFixed(2)} MXN<br/>
            <strong>Cantidad: </strong>
            <InputNumber
              min={1}
              max={this.props.part.inventory}
              onChange={quantity => this.setState({ quantity })}
              value={this.state.quantity}
              size="small"
            /><br/>
            <strong>Total a pagar: </strong>
            ${(this.props.part.price * this.state.quantity).toFixed(2)} MXN<br/>
            <Divider>Informacion de Contacto</Divider>
            <Alert message={this.contact_disclaimer} type="info" showIcon />
            <Hdiv/>
            <Input
              addonBefore="Nombre"
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
            />
            <Hdiv/>
            <Input
              addonBefore="Telefono" 
              value={this.state.phone}
              onChange={e => this.setState({ phone: e.target.value })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Correo" 
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
            />
            <Divider>Informacion de Pago</Divider>
            <Alert message={this.card_disclaimer} type="info" showIcon />
            <Hdiv/>
            <Input
              addonBefore="Nombre en la tarjeta" 
              value={this.state.card_name}
              onChange={e => this.setState({ card_name: e.target.value })} 
              onFocus={() => this.setState({ focus: 'name' })}
            />
            <Hdiv/>
            <Input
              addonBefore="Numero de tarjeta" 
              value={this.state.card_number}
              onChange={e => this.setState({ card_number: e.target.value })}
              onFocus={() => this.setState({ focus: 'number' })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Mes de expiracion" 
              value={this.state.exp_month}
              onChange={e => this.setState({ exp_month: e.target.value })} 
              onFocus={() => this.setState({ focus: 'expiry' })}
            />
            <Hdiv/>
            <Input
              addonBefore="Ano de expiracion" 
              value={this.state.exp_year}
              onChange={e => this.setState({ exp_year: e.target.value })} 
              onFocus={() => this.setState({ focus: 'expiry' })}
            />
            <Hdiv/>
            <Input
              addonBefore="Codigo de seguridad"
              value={this.state.cvc}
              onChange={e => this.setState({ cvc: e.target.value })}
              onFocus={() => this.setState({ focus: 'cvc' })}
            />
            <Hdiv/>
            <Card
              number={this.state.card_number || ""}
              name={this.state.card_name}
              expiry={this.state.exp_month + this.state.exp_year}
              cvc={this.state.cvc}
              placeholders={{ name: 'NOMBRE' }}
              focused={this.state.focus}
            />
            <Divider>Informacion de Envio</Divider>
            <Input
              addonBefore="Calle" 
              value={this.state.calle}
              onChange={e => this.setState({ calle: e.target.value })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Numero" 
              value={this.state.num}
              onChange={e => this.setState({ num: e.target.value })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Colonia" 
              value={this.state.colonia}
              onChange={e => this.setState({ colonia: e.target.value })} 
            />
            <Hdiv/>
            <Input 
              addonBefore="Codigo Postal"
              value={this.state.cp}
              onChange={e => this.setState({ cp: e.target.value })}   
            />
            <Hdiv/>
            <Input
              addonBefore="Ciudad" 
              value={this.state.ciudad}
              onChange={e => this.setState({ ciudad: e.target.value })}
            />
            <Hdiv/>
            <Input
              addonBefore="Estado"
              value={this.state.estado}
              onChange={e => this.setState({ estado: e.target.value })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Referencia" 
              value={this.state.referencia}
              onChange={e => this.setState({ referencia: e.target.value })} 
            />
            <Hdiv/>
            <Input
              addonBefore="Recibe" 
              value={this.state.recibe}
              onChange={e => this.setState({ recibe: e.target.value })} 
            />
            {this.state.card_error_msg !== "" &&
              <Alert message={this.state.card_error_msg} type="error" showIcon/>
            }
            {this.state.form_error_msg !== "" &&
              <Alert message={this.state.form_error_msg} type="error" showIcon/>
            }
          </div> : 
          <div>
            <h1 style={{textAlign: 'center'}}>
              <Icon type="check-circle" style={{ color: 'green', marginRight: '0.5em' }}/>
              Tu pago se proceso exitosamente.<br/>
            </h1>
            <Divider>Detalles</Divider>
            {this.state.payment_info.charges.data.map(charge => (
              <div>
                <strong>Total: </strong>{charge.amount}<br/>
                <strong>Tarjeta: </strong>
                {charge.payment_method.brand} **** **** **** {charge.payment_method.last4}
              </div>
            ))}
            <Divider/>
            Gracias por tu compra.<br/>
            Te estaremos contactando en los siguientes dias con tu numero de seguimiento.
          </div>
        }
      </Modal>
    )
  }
}

export default Checkout