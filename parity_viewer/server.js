const _koa = require('koa')
const { Client: _pg } = require('pg')
const group = require('lodash.groupby')
const pick = require('lodash.pick')
const serial = require('promise-serial')

const koa = new _koa()

const cache = {}

const get_parity = async (category, pg) => {
  const parts_q = `
    select
      parts.id as part_id,
      parts.title as part_title,
      parts.inventory as part_inventory,
      parts.price as part_price,
      parts.category as part_category,
      parts.image as part_image,
      parts.link as part_link,
      parts.codes as part_codes,
      parts.manufacturer as part_manufacturer,
      parts.side as part_side,
      cars.make as car_make,
      cars.model as car_model,
      cars.yearinit as car_yearinit,
      cars.yearlast as car_yearlast,
      posts.id as post_id,
      posts.title as post_title,
      posts.category as post_category,
      posts.inventory as post_inventory,
      posts.price as post_price,
      posts.premium as post_premium,
      posts.status as post_status,
      posts.description_text as post_description,
      posts.account as post_account,
      posts.make as post_make,
      posts.model as post_model,
      posts.year_init as post_yearinit,
      posts.year_last as post_yearlast
    from parts
    inner join cars on parts.id = cars.part_id
    inner join posts on parts.id = posts.part_id
  `

  let parts
  parts = await pg.query(parts_q)
  parts = parts.rows
  parts = group(parts,'part_id')
  parts = Object.values(parts)
  parts = parts.map(part_group => {
    const { part_id: id } = part_group[0]
    const { part_title: title } = part_group[0]
    const { part_inventory: inventory } = part_group[0]
    const { part_price: price } = part_group[0]
    const { part_category: category } = part_group[0]
    const { part_image: image } = part_group[0]
    const { part_link: link } = part_group[0]
    const { part_codes: codes } = part_group[0]
    const { part_manufacturer: manufacturer } = part_group[0]
    const { part_side: side } = part_group[0]

    const part = {
      id,
      title,
      inventory,
      price,
      category,
      image,
      link,
      codes,
      manufacturer,
      side,
    }

    const post_group = Object.values(group(part_group, 'post_id'))

    let posts

    posts = post_group.map(post_group => {
      const { post_id: id } = post_group[0]
      const { post_title: title } = post_group[0]
      const { post_category: category } = post_group[0]
      const { post_inventory: inventory } = post_group[0]
      const { post_price: price } = post_group[0]
      const { post_premium: premium } = post_group[0]
      const { post_status: status } = post_group[0]
      // const { post_description: description } = post_group[0]
      const { post_account: account } = post_group[0]
      const { post_make: make } = post_group[0]
      const { post_model: model } = post_group[0]
      const { post_yearinit: yearinit } = post_group[0]
      const { post_yearlast: yearlast } = post_group[0]

      const post = {
        id,
        title,
        category,
        inventory,
        price,
        premium,
        status,
        // description,
        account,
        make,
        model,
        yearinit,
        yearlast,
      }

      const cars = post_group.map(post => {
        const { car_make: make } = post
        const { car_model: model } = post
        const { car_yearinit: yearinit } = post
        const { car_yearlast: yearlast } = post
        return { make, model, yearinit, yearlast }
      })

      return { post, cars }
    })

    const cars = posts[0].cars.map(post => {
      const { make } = post
      const { model } = post
      const { yearinit } = post
      const { yearlast } = post
      return { make, model, yearinit, yearlast }
    })

    posts = posts.map(_post => {
      const { id } = _post.post
      const { title } = _post.post
      const { category } = _post.post
      const { inventory } = _post.post
      const { price } = _post.post
      const { premium } = _post.post
      const { status } = _post.post
      // const { description } = _post.post
      const { account } = _post.post
      const { make } = _post.post
      const { model } = _post.post
      const { yearinit } = _post.post
      const { yearlast } = _post.post
      
      const post = {
        id,
        title,
        category,
        inventory,
        price,
        premium,
        status,
        // description,
        account,
        make,
        model,
        yearinit,
        yearlast,
      }

      return post
    })

    return { part, posts, cars }
  })

  if (category) {
    parts = parts.filter(x => x.part.category === category)    
  }

  parts = parts.map(({ part, posts: _posts, cars: _cars }) => {
    let posts = _posts.map(post => {
      const id = post.id

      const hash = [
        post.make,
        post.model,
        // post.yearinit,
        // post.yearlast,
      ].join('-').toLowerCase().trim().replace(/ {2,}/g, ' ')

      const premium = post.premium

      return { id, hash, premium }
    })

    const cars = _cars.map(car => {
      const hash = [
        car.make,
        car.model,
        // car.yearinit,
        // car.yearlast,
      ].join('-').toLowerCase().trim().replace(/ {2,}/g, ' ')
      return hash
    })

    let parity = {}

    cars.map(car => {
      posts.map(post => {
        if (car === post.hash) {
          posts = posts.filter(p => p.hash !== car)

          if (!parity[car]) parity[car] = { p: [], s: [] }

          if (post.premium) parity[car].p.push(post.id)
          else parity[car].s.push(post.id)
        }
      })
    })

    parity['unknown'] = posts

    return ({ part, posts: _posts, cars: _cars, parity })
  })

  const unknown_posts = parts.filter(x => x.parity.unknown.length > 0)
  const extra_posts = parts.filter(x => {
    let n
    n = Object.values(x.parity).filter(x => x.p && x.s)
    n = n.map(x => Object.values(x))
    n = [].concat.apply([], n)
    n = n.map(x => x.length)
    n = n.filter(x => x > 1)
    n = n.length > 0

    return n
  })
  const missing_posts = parts.filter(x => {
    let n
    n = Object.values(x.parity).filter(x => x.p && x.s)
    n = n.map(x => Object.values(x))
    n = [].concat.apply([], n)
    n = n.map(x => x.length)
    n = n.filter(x => x === 0)
    n = n.length > 0

    return n
  })

  // let posts
  // posts = await pg.query('select * from posts')
  // posts = posts.rows
  // posts = posts.map(x => x.part_id)
  // posts = group(posts, x => x)
  // posts = Object.values(posts).map(x => x[0])

  // const not_posted = parts.filter(x => posts.indexOf('x.part.id') === -1)

  // console.log(not_posted)

  // console.log(render(sample(missing_posts)))
  console.log('# of parts:', parts.length)
  console.log('# of parts with unknows:', unknown_posts.length)
  console.log('# of parts with extras:', extra_posts.length)
  console.log('# of parts with missing:', missing_posts.length)

  return { parts, unknown_posts, extra_posts, missing_posts }
}

const get_categories = async pg => {
  const query = 'select category from parts'

  let categories
  categories = await pg.query(query)
  categories = categories.rows
  categories = categories.map(Object.values)
  categories = categories.map(x => x[0])
  
  let categories_acc = []
  for (let index = 0; index < categories.length; index++) {
    const category = categories[index];
    if (categories_acc.indexOf(category) === -1) categories_acc.push(category)
  }

  return categories_acc
}

koa.use(async ctx => {
  const pg = new _pg()
  pg.connect()

  ctx.set("Access-Control-Allow-Origin", "*")
  ctx.set("Access-Control-Allow-Credentials", "true")
  ctx.set("Access-Control-Allow-Headers", "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With")
  ctx.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

  if (ctx.method === 'GET' && ctx.path === '/parity') {
    ctx.body = await get_parity(ctx.query.category, pg)
  }
  else if (ctx.method === 'GET' && ctx.path === '/categories') {
    ctx.body = await get_categories(pg)
  }
  else {
    ctx.status = 501
  }
})

const port = 8001
koa.listen(port, () => console.log('listening on port', port))