const _koa = require('koa')
const { Client: _pg } = require('pg')
const group = require('lodash.groupby')
const pick = require('lodash.pick')
const serial = require('promise-serial')

const koa = new _koa()

const cache = {}

const get_parts = async (env, pg) => {
  let make
  make = env.make
  if (env.make === 'Todos') make = null

  let model
  model = env.model
  if (env.model === 'Todos') model = null

  let category
  category = env.category
  if (env.category === 'Todos') category = null

  const year = Number(env.year) || null
  
  const limit = Number(env.limit) || 50

  const offset = Number(env.offset) || 0

  console.log({ make, model, year, category, limit, offset })

  const hash = Object.values(env).join('-').replace(/ /g, '/')

  const query = `
    select * from cars
    inner join
        parts on parts.id = cars.part_id
    where
      parts.vendor = 'AL'
      and ($1::text is null or cars.make = $1)
      and ($2::text is null or cars.model = $2)
      and ($3::int is null or $3 between cars.yearinit and cars.yearlast)
      and ($4::text is null or parts.category = $4)
  `

  let parts
  parts = await pg.query(query, [make, model, year, category])
  parts = parts.rows
  parts = group(parts,'id')
  parts = Object.values(parts)
  parts = parts.map(p => {
    const cars = p.map(x => pick(x, ['make', 'model', 'yearinit', 'yearlast']))
    const { make, model, yearinit, yearlast, ...part } = p[0]
    return { ...part, cars }
  })
  parts = parts.slice(offset, limit + offset)

  cache[hash] = parts
  console.log(parts.length)

  return parts
}

const get_categories = async pg => {
  const query = 'select category from parts where vendor = \'AL\''

  let categories
  categories = await pg.query(query)
  categories = categories.rows
  categories = categories.map(Object.values)
  categories = categories.map(x => x[0])
  
  let categories_acc = []
  for (let index = 0; index < categories.length; index++) {
    const category = categories[index];
    if (categories_acc.indexOf(category) === -1) categories_acc.push(category)
  }

  return categories_acc
}

const get_model = async (pg, make) => {
  const query = `
    select model 
    from cars
    where
      make = $1
      and vendor = 'AL'
  `

  let models
  models = await pg.query(query, [make])
  models = models.rows
  models = models.map(Object.values)
  models = models.map(x => x[0])
  
  let models_acc = []
  for (let index = 0; index < models.length; index++) {
    const model = models[index];
    if (models_acc.indexOf(model) === -1) models_acc.push(model)
  }

  return models_acc
}

const get_models = async pg => {
  let makes
  makes = await pg.query('select make from cars where vendor = \'AL\'')
  makes = makes.rows
  makes = makes.map(Object.values)
  makes = makes.map(x => x[0])
  
  let makes_acc = []
  for (let index = 0; index < makes.length; index++) {
    const make = makes[index];
    if (makes_acc.indexOf(make) === -1) makes_acc.push(make)
  }
  
  makes = makes_acc

  let models
  models = makes.map(make => () => get_model(pg, make).then(models => ({ make, models })))
  models = await serial(models)

  return models
}

koa.use(async ctx => {
  const pg = new _pg()
  pg.connect()

  ctx.set("Access-Control-Allow-Origin", "*")
  ctx.set("Access-Control-Allow-Credentials", "true")
  ctx.set("Access-Control-Allow-Headers", "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With")
  ctx.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

  if (ctx.method === 'GET' && ctx.path === '/parts') {
    ctx.body = await get_parts(ctx.query, pg)
  }
  else if (ctx.method === 'GET' && ctx.path === '/categories') {
    ctx.body = await get_categories(pg)
  }
  else if (ctx.method === 'GET' && ctx.path === '/models') {
    ctx.body = await get_models(pg)
  }
  else {
    ctx.status = 501
  }
})

koa.listen(8000, () => console.log('listening on :8000'))