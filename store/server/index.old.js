const http = require('http')
const { parse: parse_url } = require('url')
const to_pairs = require('lodash.topairs')
const { parse: parse_qs } = require('querystring')
const _sqlite = require('sqlite')
const serial = require('promise-serial')
const conekta = require('conekta')

conekta.api_key = 'key_eYvWV7gSDkNYXsmr'
conekta.api_version = '2.0.0'

// the open method is asyncronous, could not be ready when used ?
let sqlite
_sqlite.open('../data/processed.sqlite').then(x => sqlite = x)

const cache = {}

const get_parts = async env => {
  const { make } = env
  const { model } = env
  const { year } = env
  const { category } = env
  const { offset } = env

  // todo: null check for all params here

  const hash = Object.values(env).join('-').replace(/ /g, '/')

  if(cache[hash]) {
    console.log('from cache: ', hash)
    return cache[hash]
  }

  if ( make === 'Todos' & model === 'Todos' & year === 'Todos') {
    let query = []
    query[0] = 'select * from parts'
    query[1] = 'where vendor = \'AL\''
    query[2] = category !== 'Todos' ? 'and category = ?' : null
    query[3] = ' limit 50 offset ?'
    query = query.filter(x => x !== null)
    query = query.join(' ')

    const values = category !== 'Todos' ? [ category, offset ] : [ offset ]

    console.log(query, values)

    const parts = await sqlite.all(query, values)

    cache[hash] = parts

    return parts
  }
  else {
    if (env.year !== 'Todos') env.year = parseInt(env.year)

    let pairs
    pairs = to_pairs(env)
    pairs = pairs.filter(x => x[1] !== 'Todos')
    pairs = pairs.filter(x => x[0] !== 'year')
    pairs = pairs.filter(x => x[0] !== 'offset')

    const keys = pairs.map(x => x[0])

    let values
    values = pairs.map(x => x[1])
    if (env.year !== 'Todos') {
      values = values.concat([ env.year, env.year, offset ])
    }
    else {
      values = values.concat([ offset ])
    }

    let parts_q
    parts_q = [
      'select * from cars',
      'inner join parts on parts.id = cars.part_id',
      'where parts.vendor = \'AL\'',
      keys.length === 0 ? null : 'and',
      keys.map(k => `${k} = ?`).join(' and '),
    ]
    parts_q = parts_q.filter(x => x !== null)
    parts_q = parts_q.join(' ')
    if (env.year !== 'Todos') parts_q += ' and cars.yearinit <= ? and cars.yearlast >= ?'
    parts_q += ' limit 50 offset ?'

    console.log(parts_q, values)

    const parts = await sqlite.all(parts_q, values)

    cache[hash] = parts

    return parts
  }
}

const get_makes = async () => {
  const makes_q = 'select make from cars where vendor = \'AL\''

  let makes
  makes = await sqlite.all(makes_q)
  makes = makes.map(Object.values)
  makes = makes.map(x => x[0])
  
  let makes_acc = []
  for (let index = 0; index < makes.length; index++) {
    const make = makes[index]
    if (makes_acc.indexOf(make) === -1) makes_acc.push(make)
  }
  
  return makes_acc
}

const get_model = async make => {
  let models
  models = await sqlite.all('select model from cars where make = ? and vendor = \'AL\'', make)
  models = models.map(Object.values)
  models = models.map(x => x[0])
  
  let models_acc = []
  for (let index = 0; index < models.length; index++) {
    const model = models[index];
    if (models_acc.indexOf(model) === -1) models_acc.push(model)
  }

  return models_acc
}

const get_models = async () => {
  let makes
  makes = await sqlite.all('select make from cars where vendor = \'AL\'')
  makes = makes.map(Object.values)
  makes = makes.map(x => x[0])
  
  let makes_acc = []
  for (let index = 0; index < makes.length; index++) {
    const make = makes[index];
    if (makes_acc.indexOf(make) === -1) makes_acc.push(make)
  }
  
  makes = makes_acc

  let models
  models = makes.map(make => () => get_model(make).then(models => ({ make, models })))
  models = await serial(models)

  return models
}

const get_categories = async () => {
  const query = 'select category from parts where vendor = \'AL\''

  let categories
  categories = await sqlite.all(query)
  categories = categories.map(Object.values)
  categories = categories.map(x => x[0])
  
  let categories_acc = []
  for (let index = 0; index < categories.length; index++) {
    const category = categories[index];
    if (categories_acc.indexOf(category) === -1) categories_acc.push(category)
  }

  return categories_acc
}

const create_costumer = args => new Promise((resolve, reject) => {
  conekta.Customer.create(args, function(err, res) {
      if(err) reject(err)
      else resolve(res.toObject())
  })
})

const create_order = args => new Promise((resolve, reject) => {
  order = conekta.Order.create(args, function(err, res) {
      if(err) reject(err)
      else resolve(res.toObject())
  })
})

const pay = async args => {
  const { token } = args
  const { name, phone, email } = args
  const { part_id } = args
  const quantity = parseInt(args.quantity)
  const { calle, num, colonia, cp, ciudad, estado, referencia, recibe } = args

  let part
  part = await sqlite.get('select * from parts where id = ?', part_id)
  part.price = parseFloat(part.price)

  console.log(args)
  console.log()
  console.log(part)

  const order_args = {
    currency: 'MXN',
    line_items: [{ name: part.title, unit_price: part.price, quantity }],
    customer_info: { name, phone, email },
    metadata: { part_id, calle, num, colonia, cp, ciudad, estado, referencia, recibe },
    charges:[{
      ammount: part.price * quantity,
      payment_method: { type: 'card', token_id: token },
    }],
  }

  const order = await create_order(order_args)

  return order

  // return { id: order.id, status: order.payment_status }
}

const server = http.createServer((req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader("Access-Control-Allow-Credentials", "true")
  res.setHeader("Access-Control-Allow-Headers", "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With")
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

  const url = parse_url(req.url)

  if (url.pathname === '/parts') {
    res.setHeader('Content-Type', 'application/json')
    const opts = parse_qs(url.query)
    get_parts(opts).then(x => res.end(JSON.stringify(x)))
  }
  else if (url.pathname === '/categories') {
    res.setHeader('Content-Type', 'application/json')
    get_categories().then(x => res.end(JSON.stringify(x)))
  }
  else if (url.pathname === '/makes') {
    res.setHeader('Content-Type', 'application/json')
    get_makes().then(x => res.end(JSON.stringify(x)))
  }
  else if (url.pathname === '/models') {
    res.setHeader('Content-Type', 'application/json')

    get_models(parse_qs(url.query))
      .then(x => res.end(JSON.stringify(x)))
  }
  else if (url.pathname === '/pay') {
    res.setHeader('Content-Type', 'application/json')

    pay(parse_qs(url.query))
      .then(x => res.end(JSON.stringify(x)))
  }
  else {
    res.end('use /parts for searching parts')
  }
})

server.listen(8000)