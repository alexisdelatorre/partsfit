import logo from './logo.png'

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled, { injectGlobal } from 'styled-components'

import { Divider, Icon } from 'antd'
import BuyGrid from './buy_grid'

injectGlobal`
  body {
    background-color: #fbfbfb;
  }
`

const Wrapper = styled.div`
  padding: 0 50px;

  @media (min-width: 900px) {
    padding: 0 100px;
  }

  @media (min-width: 1400px) {
    max-width: 1400px;
    margin: auto;
    margin-top: 35px;
  }
`

const Logo = styled.img`
  height: 100px;
  margin-top: 2em;
  margin-bottom: 1em;
`

class Index extends Component {
  render(){
    return (
      <Wrapper>
        <a href="/"><Logo src={logo}/></a>
        <Divider/>
        <div style={{display: 'flex', justifyContent: "center", alignItems: 'flex-end', marginTop: '40px'}}>
          <p style={{marginRight: '1em'}}><Icon type="compass"/> <strong>Envio gratis a toda la republica</strong></p>
          <p><Icon type="phone"/> <strong>Atencion a Clientes: (686) 251-2452</strong></p>
        </div>
        <Divider/>
        <BuyGrid/>
      </Wrapper>
    )
  }
}

ReactDOM.render(<Index/>, document.getElementById('root'))