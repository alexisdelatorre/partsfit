import 'antd/dist/antd.css'

import React, { Component } from 'react'
import styled from 'styled-components'
import { get } from 'axios'

import { Select as _Select } from 'antd'
import { Button } from 'antd'
import { Cascader as _Cascader } from 'antd'
import { Card as _Card } from 'antd'

import Checkout from './checkout'

const Option = _Select.Option

const host = 'http://192.168.0.4:8000'

const Parts = styled.div`
  @media (min-width: 750px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 1em;
  }

  @media (min-width: 1000px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 1em;
  }

  @media (min-width: 1400px) {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 1em;
  }
`

const Card = styled(_Card)`
  margin-bottom: 1em;

  @media (min-width: 750px) {
    margin-bottom: 0px;
  }
`

const Label = styled.span`
  margin-right: 10px
`

const Select = styled(_Select)`
  min-width: 220px;
`

const Cascader = styled(_Cascader)`
  min-width: 220px;
`

const Filters = styled.div`
  margin-top: 2em;

  @media (min-width: 1200px) {
    display: flex;
    margin-bottom: 2em;
  }
`

const Filter = styled.div`
  margin-right: 1em;

  @media (max-width: 1200px) {
    display: flex;
    align-items: center;
    margin-bottom: 2em;
    justify-content: center;
  }
`

const Image = styled.div`
  width: 100px;
  height: 100px;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  background-size: contain;
  background-color: white;
  margin-bottom: 16px;
`

class BuyGrid extends Component {
  state = {
    make: 'Todos',
    model: 'Todos',
    category: 'Todos',
    year: 'Todos',
    last_year: 'Todos',
    data: [],
    loading: false,
    categories: [ 'Todos' ],
    makes: [ 'Todos' ],
    models: [ { value: 'Todos', label: 'Todos' } ],
    offset: 0,
    end: false,
    nothing: false,
    modal_visible: false,
    modal_image: '',
    show_checkout: false,
    part: {
      id: "",
      title: "",
      price: 0,
      inventory: 0,
    },
  }

  constructor(props) {
    super(props)

    get(`${host}/categories`)
      .catch(console.log)
      .then(res => res.data)
      .then(c => ['Todos'].concat(c))
      .then(categories => this.setState({ categories }), this.update_table)

    get(`${host}/models`)
      .then(res => {
        const cars = res.data

        const options = cars.map(({ make, models }) => ({
          value: make,
          label: make,
          children: models.map(model => ({
            value: model,
            label: model,
          })),
        }))

        const def = [ { value: 'Todos', label: 'Todos' } ]

        this.setState({ models: def.concat(options) }, this.update_table)
      })
  }

  update_table = () => {
    this.setState({ loading: true })
    
    const link = [
      `${host}/parts`,
      `?make=${this.state.make}`,
      `&model=${this.state.model}`,
      `&year=${this.state.year}`,
      `&category=${this.state.category}`,
      `&offset=${this.state.offset}`,
    ].join('')

    get(link).then(res => {
      const parts = res.data

      if (parts.length === 0 || parts.length < 50) {
        this.setState({ end: true })
      }

      if (this.state.data.length === 0 && parts.length === 0) {
        this.setState({ nothing: true })
      }

      const data = parts.map((part, idx) => ({
        key: `${part.id}-${idx}`,
        id: part.id,
        title: part.title,
        image: part.image.replace('aldoautopartesproductos/', 'aldoautopartesproductos/_thumbs/'),
        inventory: part.inventory < 5 ? 0 : part.inventory,
        price: part.price,
        buy: '',
      }))

      this.setState({ data: this.state.data.concat(data) })
      this.setState({ loading: false })
    })
  }

  model_change = (value) => {
    this.setState({ data: [], offset: 0, end: false, nothing: false })
    if (value.length === 1 && value[0] === 'Todos') {
      const state = { make: 'Todos', model: 'Todos' }
      this.setState(state, this.update_table)
    }
    else {
      const state = { make: value[0], model: value[1] }
      this.setState(state, this.update_table)
    }
  }

  category_change = (value) => {
    const state = {
      category: value,
      data: [],
      offset: 0,
      end: false,
      nothing: false,
    }
    this.setState(state, this.update_table)
  }

  year_change = (value) => {
    const state = {
      year: value, 
      data: [], 
      offset: 0, 
      end: false, 
      nothing: false,
    }
    this.setState(state, () => this.update_table())
  }

  load_more = () => {
    this.setState({ offset: this.state.offset + 50 }, this.update_table)
  }

  buy_click = part => () => {
    this.setState({ part, show_checkout: true })
  }

  cancel_buy = () => {
    this.setState({ show_checkout: false })
  }

  render() {
    const categories = this.state.categories
    const models = this.state.models
    const loading = this.state.loading

    const data = this.state.data

    return (
      <div>
        <Filters>
          <Filter>
            <Label>Categoria:</Label>
            <Select defaultValue={"Todos"} onChange={this.category_change}>
              {categories.map(cat => <Option value={cat}>{cat}</Option>)}
            </Select>
          </Filter>
          <Filter>
            <Label>Modelo:</Label>
            <Cascader options={models} defaultValue={['Todos']} onChange={this.model_change}/>
          </Filter>
          <Filter>
            <Label>Año:</Label>
            <Select defaultValue={"Todos"} onChange={this.year_change}>
              {['Todos'].concat(Array.from({ length: 60 }, (v, k) => 2019 - k)).map(year => {
                return <Option value={year}>{year}</Option>
              })}
            </Select>
          </Filter>
        </Filters>
        <Parts>
          {data.map(part => (
            <Card title={part.id}>
              <p>{part.title}</p>
              <a href={part.image.replace('_thumbs/', '')}>
                <Image style={{backgroundImage: `url('${part.image}')`}}></Image>
              </a>
              <p>${part.price}.00 MXN - {part.inventory < 5 ? 0 : part.inventory} Disponibles</p>
              <Button 
                disabled={part.inventory < 5}
                onClick={this.buy_click(part)}
              >
                Comprar
              </Button>
            </Card>
          ))}
        </Parts>
        {!this.state.end && !this.state.loading &&
          <div style={{textAlign: 'center', marginBottom: '2em', marginTop: '2em'}}>
            <Button
              size="large"
              disabled={loading}
              onClick={this.load_more}
              type="primary"
            >
              Cargar Mas Partes
            </Button>
          </div>
        }
        {this.state.nothing && <p>No se encontraron piezas para esta busqueda</p>}
        <Checkout
           part={this.state.part}
           visible={this.state.show_checkout}
           onCancel={this.cancel_buy}
        />
      </div>
    )
  }
}

export default BuyGrid