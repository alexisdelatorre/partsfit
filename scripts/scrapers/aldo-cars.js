require('dotenv').config()

const Postgres = require('pg').Client
// const get = require('axios').get
const load = require('cheerio').load
const sample = require('lodash.samplesize')
const rr = require('rr')
const request = require('request-promise-native')
const range = require('lodash.range')
const serial = require('promise-serial')

const dev = process.env.NODE_ENV !== 'production'

const host = 'http://www.aldoautopartes.com'

const proxies = process.env.PROXIES.split(',')

function nextProxy () {
    const proxy = 'http://'
        + process.env.PROXY_USER
        + ':'
        + process.env.PROXY_PASS
        + '@'
        + rr(proxies)
    return proxy
}

;
(async function () {
    const pg = new Postgres({ database: 'partsfit' })
    await pg.connect()

    if (!dev) await pg.query('delete from cars where vendor_id = $1', ['AL'])

    const html = await request(
        `${host}/pi_busqueda.jsp`,
        { proxy: nextProxy() }
    )

    const $ = load(html)

    let makes = []
    $('#id_marca > option').each((i, el) => {
        const name = $(el).text()
        const id = $(el).attr('value')

        if (name === '' || name === '-') return
        else makes.push({ name, id })
    })

    if (dev) makes = sample(makes)

    let models = []
    for (let i = 0; i < makes.length; i++) {
        const make = makes[i]

        const html = await request(
            `${host}/pi_busqueda.jsp?id_marca=${make.id}`,
            { proxy: nextProxy() }
        )

        const $ = load(html)
        
        let models = []
        $('#id_modelo > option').each((i, el) => {
            const name = $(el).text()
            const id = $(el).attr('value')
        
            if (name === '' || name === '-') return
            else models.push({ make: make.name, name, id })
        })

        if (dev) models = sample(models)

        for (let i = 0; i < models.length; i++) {
            const model = models[i]
            
            const year_from = 1970
            const year_to = 2020

            const years = range(year_from, year_to)
            // years_total = year_to - year_from

            let parts_acc = []

            // for (let i = 0; i < years.length; i++) {
            //     const year = years[i]

            await serial(years.map(year => async () => {
                let tries = 1
  
                const form = {
                    _action: 1,
                    anio: year,
                    id_marca: make.id,
                    id_modelo: model.id,
                }

                let html

                while (true) {
                    if (tries > 5) {
                        console.log('to many retries', tries)
                        html = ''
                        break
                    }
    
                    // console.log(`[${make.name} ${model.name} ${year}] iteration`, tries)

                    try {
                        html = await request({
                            url: `${host}/pi_resultados.jsp`,
                            timeout: 10000,
                            proxy: nextProxy(),
                            form,
                            method: 'POST',
                        })

                        break
                    } catch (error) {
                        console.log(`[${make.name} ${model.name} ${year}]`, error.error.code)
                        tries++
                        continue
                    }
                }

                if (html === '') {
                    console.log('to many retries')
                    return
                }
                
                const $ = load(html)

                const selector = $('.tablegridcella,.tablegridcellb')

                let parts = []

                selector.each((i, el) => {
                    const id = $($(el).find('td').get(0)).text()
                    parts.push({ id, year })
                })

                for (let i = 0; i < parts.length; i++) {
                    const part = parts[i]

                    if (!parts_acc[part.id]) {
                        parts_acc[part.id] = {
                            id: part.id,
                            make: make.name,
                            model: model.name,
                            year_init: year, 
                            year_last: year,
                        }
                    }
                    else {
                        if (year < parts_acc[part.id].year_init) {
                            parts_acc[part.id].year_init = year
                        }
                        
                        if (year > parts_acc[part.id].year_last) {
                            parts_acc[part.id].year_last = year
                        }
                    }
                }

                console.log(`[${make.name} ${model.name} ${year}]`, 'Found:', parts.length, 'iteration:', tries)
            }), { parallelize: 5 })
                

            parts_acc = Object.values(parts_acc)
            console.log(`[${make.name} ${model.name}]`, `total: ${parts_acc.length} parts`)

            for (let i = 0; i < parts_acc.length; i++) {
                const part = parts_acc[i]
                
                const values = [
                    part.id,
                    'AL',
                    part.make,
                    part.model,
                    part.year_init,
                    part.year_last,
                ]
        
                const query = `
                    insert into cars values 
                    (${range(1, values.length + 1).map(x => '$' + x)})
                `
        
                if (!dev) await pg.query(query, values)
                else console.log(query, values)
            }
        }
    }
})().catch(console.log)