const { post } = require('axios')
const { fromString: to_text } = require('html-to-text')

const host = 'http://www.radec.com.mx'

const login = async (_user, _pass) => {
  const env_user = process.env.RADEC_USER
  const env_pass = process.env.RADEC_PASSWORD

  const no_user_err = 'pass an user or set env variable RADEC_USER'
  const no_pass_err = 'pass a password or set env variable RADEC_PASSWORD'

  let user
  if (_user) user = _user
  else if (env_user) user = env_user
  else throw new Error(no_user_err)

  let pass
  if (_pass) pass = _pass
  else if (env_pass) pass = env_pass
  else throw new Error(no_pass_err)

  const link = `${host}/inicio?destination=node/2`
  
  let params = {}
  params['form_build_id'] = 'form-qo70MuIf6st4CNGQs4iuuchNyzA2srkBIU_Nm4C6DeY'
  params['form_id'] = 'user_login_block'
  params['name'] = user
  params['op'] = 'Iniciar+sesión'
  params['pass'] = pass

  const res = await post(link, params)

  let cookie
  cookie = res.headers['set-cookie'][0].split(';')[0]
  cookie = cookie + '; DRUPAL_UID=1;'

  console.log(res)

  return cookie
}

const init = async () => {
  let header
  header = await login()
  header = { headers: { Cookie: header } }

  console.log(header)

  // const res = await get(host, header)

  // console.log(to_text(res.data))
}

init()

