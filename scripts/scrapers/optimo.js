const { readFileSync: read_file } = require('fs')
const { csvParse: parse_csv } = require('d3-dsv')
const { Client: _pg } = require('pg')
const { get } = require('axios')
const sample = require('lodash.samplesize')
const { load } = require('cheerio')
const parallel = require('../../utils/parallel')
const R = require('ramda')
//migration
const range = require('lodash.range')

let count = 0
let total

const dev = process.env.NODE_ENV !== 'production'

const host = 'http://www.optimoautopartes.mx'

const side = title => {
  if (title.includes(' DER')) return 'right'
  else if (title.includes(' IZQ')) return 'left'
  else return 'na'
}

const get_categories = async () => {
  let html
  // html = yield encaseP(get, `${host}/productos.php`)
  html = await get(`${host}/productos.php`)
  html = html.data

  const $ = load(html)

  const selector = $('#DataTables_Table_0_wrapper td:nth-child(1) tr > td > a')

  let categories = []

  selector.each(function () {
    const a = $(this)

    const name = a.text()

    let id
    id = a.attr('onclick')
    id = id.replace('cargaCategorias(', '')
    id = id.replace(')', '')
    id = id.split(', ')[0]

    let category = { name, id }

    categories.push(category)
  })

  return categories
}

const get_products = async category => {
  const link = [
    host,
    '/inc_avki',
    '/dependencia.php',
    '?mod=cargaQueryCombos',
    '&opcion=0',
    `&opcion2=${category}`,
    '&opcion3=0',
    '&opcion4=0',
    '&opcion5=0',
    '&opcion6=0',
  ].join('')

  let html
  html = await get(link)
  html = html.data

  let products
  // does this need to be an Array?
  products = html.match(/<tr.*?>.*?<\/tr>/g)
  products = products.map(x => x.replace(/<tr>|<\/tr>/g, ''))
  products = products.map(x => x.replace(/<tr>|<\/tr>/g, ''))
  products = products.map(x => x.replace(/<td>/g, ''))
  products = products.map(x => x.split('</td>'))
  products = products.map(x => {
    let id
    id = x[0]
    id = id.replace(/<tr id='.*'>|<\/a>/g, '')

    const make = x[2]

    const model = x[3]

    const years = x[4].split(' al ')

    const title = x[5]

    const codes = x[6]

    const manufacturer =
    x[9].replace(/<img (.*?) alt=\'|\'>/g, '')

    return { id, make, model, years, title, codes, manufacturer }
  })

  return products
}

const run = async (env, category) => {
  const log = _message => {
    count += 1

    const percent =
    x => (x*100).toFixed(2)

    const message = [
      `[${count}/${total}]`,
      `[${percent(count/total)}%]`,
      `[${category.name}]`,
      `${_message}\n`,
    ]

    console.log(message.join(' '))
  }

  const { pg, catalog } = env

  // get products

  let products
  products = await get_products(category.id)
  if (dev) products = sample(products, 10)
  products = products.map(product => {
    const c = R.find(R.propEq('id', product.id), catalog)

    if (!c) return null

    return {
      vendor: 'OP',
      id: product.id,
      title: product.title,
      inventory: c.inventory,
      price: c.price,
      category: category.name,
      image: `${host}/imgcat/${product.id}big.jpg`,
      link: `${host}/productos.php`,
      codes: product.codes,
      manufacturer: product.manufacturer,
      side: side(product.title),
      car: {
        make: product.make,
        model: product.model,
        yearinit: product.years[0],
        yearlast: product.years[1],
      },
    }
  })

  products = products.filter(x => x !== null)

  let cars = []

  products = products.map(
    ({ car, ...product }) => {
      cars.push({
         part_id: product.id,
         vendor: product.vendor,
        ...car,
      })
      return product
    }
  )

  if (dev) products = sample(products)

    // migration
    for (let i = 0; i < products.length; i++) {
        const product = products[i]

        const side = product.side !== 'na' 
            ? product.side : null

        const values = [
            product.id,
            product.vendor,
            product.title,
            product.inventory,
            product.price,
            product.category,
            product.image,
            null,
            product.codes,
            product.manufacturer,
            side,
        ]

        const query = `
            insert into parts values 
            (${range(1, values.length + 1).map(x => '$' + x)})
        `

        if (!dev) await pg.query(query, values)
        else console.log(query, values)
    }

    //migration
    for (let i = 0; i < cars.length; i++) {
        const car = cars[i]
        
        const values = [
            car.part_id,
            car.vendor,
            car.make,
            car.model,
            car.yearinit,
            car.yearlast,
        ]

        const query = `
            insert into cars values 
            (${range(1, values.length + 1).map(x => '$' + x)})
        `

        if (!dev) await pg.query(query, values)
        else console.log(query, values)
    }

  products = products.map(p => [
    p.vendor,
    p.id,
    p.title,
    p.inventory,
    p.price,
    p.category,
    p.image,
    p.link,
    p.codes,
    p.manufacturer,
    p.side,
  ])

  const found = products.length

//   if (!dev) {
//     try {
//       await pg.query('begin')
//       const insert_q = 'insert into parts values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)'
//       parallel(products.map(p => () => pg.query(insert_q, p)), 20)
//       await pg.query('commit')
//     } catch (e) {
//       await pg.query('rollback')
//     }
//   }
//   else {
//     const insert_q = 'insert into parts values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)'
//     // console.log(insert_q, products)
//   }

  cars = cars.map(c => [
    c.part_id,
    c.vendor,
    c.make,
    c.model,
    c.yearinit,
    c.yearlast
  ])

//   if (!dev) {
//     try {
//       await pg.query('begin')
//       const insert_q = 'insert into cars values ($1, $2, $3, $4, $5, $6)'
//       parallel(cars.map(c => () => pg.query(insert_q, c)), 20)
//       await pg.query('commit')
//     } catch (e) {
//       await pg.query('rollback')
//     }
//   }
//   else {
//     const insert_q = 'insert into cars values ($1, $2, $3, $4, $5, $6)'
//     // console.log(insert_q, cars)
//   }

  // console.log(cars)

  log(`${found} parts found`)
}

const parse_catalog = d => ({
  id: d.id,
  price: parseInt(d.price), 
  inventory: parseInt(d.inventory)
});

(async () => {
  let catalog
  catalog = read_file('data/inventory_optimo.csv', 'utf-8')
  catalog = await parse_csv(catalog, parse_catalog)

  const pg = new _pg({ database: 'partsfit' })
  await pg.connect()

  const create_parts_q = `
    create table if not exists parts (
      vendor text,
      id text,
      title text,
      inventory int,
      price numeric,
      category text,
      image text,
      link text,
      codes text,
      manufacturer text,
      side text
    )
  `

  const clear_parts_q = `delete from parts where vendor = 'OP'`

//   if (!dev) {
//     await pg.query(clear_parts_q).catch(() => {})
//     await pg.query(create_parts_q).catch(() => {})
//   }
//   else {
//     // console.log(clear_parts_q)
//     // console.log(create_parts_q)
//   }

//   const create_cars_q = `
//   create table if not exists cars (
//       part_id text,
//       vendor text,
//       make text,
//       model text,
//       yearinit int,
//       yearlast int
//     )
//   `

//   const clear_cars_q = `delete from cars where vendor = 'OP'`

//   if (!dev) {
//     await pg.query(clear_cars_q).catch(() => {})
//     await pg.query(create_cars_q).catch(() => {})
//   }
//   else {
//     // console.log(clear_cars_q)
//     // console.log(create_cars_q)
//   }

  // migration
  if (!dev) {
    await pg.query('delete from cars where vendor_id = $1', ['OP'])
    await pg.query('delete from parts where vendor_id = $1', ['OP'])
  }

  const env = { pg, catalog }

  let categories = await get_categories()
  if (dev) categories = [{ name: 'ASPAS', id: '6' }]

  total = categories.length

  parallel(categories.map(cat => () => run(env, cat)), 1)
})().catch(console.log)