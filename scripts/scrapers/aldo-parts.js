require('dotenv').config()

// const axios = require('axios')
// const request = require('request-promise')
const { load } = require('cheerio')
const sample = require('lodash.samplesize')
const serial = require('promise-serial')
const logUpdate = require('log-update')
const Postgres = require('pg').Client
const rr = require('rr')
const _request = require('request')
const promisify = require('util').promisify
const range = require('lodash.range')

const dev = process.env.NODE_ENV !== "production"

const host = 'http://www.aldoautopartes.com'

const proxies = process.env.PROXIES.split(',')

const request = promisify(_request)

const http_get = (url, proxy) => {
  if (proxy) return request({ url, proxy })
  else return request({ url })
}

// const get = link => new Promise((resolve, reject) => {

//   const max_retries = 100

//   const res = async () => {
//     const proxy = 'http://' + rr(proxies)
//     console.log('get:', link, proxy)

//     let count = 0

//     setTimeout(() => {
//       if (count < max_retries) {
//         reject('to many retries:', max_retries)
//       }
//       else {
//         console.log('timeout, retry')
//         count += 1
//         res()
//       }
//     }, 120000)

//     request({ url: link, proxy })
//       .then(data => resolve({data}))
//       .catch(e => {})
//   }

//   res()

//   // resolve({ data })
// })

const get = async url => {
  const proxy = 'http://' + rr(proxies)
  const data = await request({ url, proxy })
  return { data }
}

let pg

// for progress logging
let categories_total
let categories_count = 0
let makes_total
let makes_count = 0
let parts_total
let parts_count = 0
let percent = 0

const parseSide = title => {
  if (title.includes(' DER')) return 'right'
  else if (title.includes(' IZQ')) return 'left'
  else return 'na'
}

const get_categories = async () => {
  console.log('Scraping categories')

  const html = await request(`${host}/pi_inicio.jsp`)
    .then(x => x.body)

  const $ = load(html)

  const selector = $('table:nth-child(3) td:nth-child(2) tr > td > a')

  let categories = []

  selector.each(function () {
    const name = $(this).text()

    let id
    id = $(this).attr('href')
    id = id.replace('pi_busqueda.jsp?id_articulotipo=', '')

    categories.push({ name, id })
  })

  console.log(`Found ${categories.length}:`)
  categories.map(c => console.log(`  - ${c.name}`))

  return categories
}

const get_makes = async category_id => {
  const error_msg = 'There was an error getting the makes'
  
  console.log('Getting makes')

  const link = `${host}/pi_busqueda.jsp?id_articulotipo=${category_id}`

  let html
  try {
    html = await request(link).then(x => x.body)
  } catch (e) {
    console.log(error_msg)
    console,log(JSON.stringify(e, null, 4))
  }

  const $ = load(html)

  const selector = $('table:nth-child(4) tr > td > a')

  let makes = []

  selector.each(function () {
    const name = $(this).text()

    const to_replace = 
    `pi_resultados.jsp?id_articulotipo=${category_id}&id_articulogrupo=`

    let id
    id = $(this).attr('href')
    id = id.replace(to_replace, '')

    makes.push({ name, id })
  })

  console.log(`Found ${makes.length}:`)
  makes.map(m => console.log(`  - ${m.name}`))

  return makes
}

const parse_parts = async (html) => {
  const $ = load(html)

  const selector = $('.tablegridcella,.tablegridcellb')
    
  let parts = []
    
  selector.each(function () {
    const id = $($(this).find('td').get(0)).text()

    const codes = $($(this).find('td').get(1)).text()

    let image
    image = $($(this).find('td').get(2)).find('img').attr('src')
    image = image.replace('/_thumbs', '')

    let link
    link = $($(this).find('td').get(3)).find('a').attr('href')
    link = `${host}/${link}`

    const title = $($(this).find('td').get(3)).text()

    let price
    price = $($(this).find('td').get(4)).text()
    price = price.replace(/\$|,/g, '')
    price = parseFloat(price)

    let inventory
    inventory = $($(this).find('td').get(5)).text()
    inventory = inventory === 'Mas de 60' ? 60 : parseInt(inventory)

    parts.push({ id, codes, link, image, title, price, inventory })
  })

  return parts
}

const get_parts = async category => {
  console.log(`[${category.name}]`, 'Processing Category')
  
  let makes
  makes = await get_makes(category.id)
  if (dev) {
    makes = sample(makes)
    console.log('Dev ENV, chosing random make:', makes[0].name)
  }
  // makes = makes.filter(x => x.name === 'Dodge / Chrysler')

  // for (let i = 0; i < makes.length; i++) {
    
  // }

  await serial(makes.map(make => async () => {
    // counter that increments everytime a request fails or timeout
    let tries = 1

    while (true) {
      const proxy = 'http://'
        + process.env.PROXY_USER
        + ':'
        + process.env.PROXY_PASS
        + '@'
        + rr(proxies)

      console.log(`[${make.name}]`, `iteration ${tries}, proxy:`, proxy)

      if (tries > 10) {
        console.log(`[${make.name}]`, 'To many retries:', tries)
        break
      }

      const url = [
        host,
        '/pi_resultados.jsp',
        `?id_articulotipo=${category.id}`,
        `&id_articulogrupo=${make.id}`,
      ].join('')

      let html

      const timeout = 5000

      try {
        html = await request({ url, timeout, proxy }).then(x => x.body)
      } catch (e) {
          if (e.code === 'ETIMEDOUT') {
            console.log(`[${make.name}]`, 'Timeout, tries:', tries)
          }
          else {
            console.log(`[${make.name}]`, 'Unknown error:')
            console.log(e)
          }
          tries += 1
          continue
      }

      console.log(`[${make.name}]`, 'Parsing')

      const parts = await parse_parts(html, category)

      console.log(`[${make.name}]`, 'Found:', parts.length)

      for (let i = 0; i < parts.length; i++) {
          const product = parts[i]

          const side = parseSide(product.title) !== 'na' 
              ? parseSide(product.title) : null

          const values = [
              product.id,
              'AL',
              product.title,
              product.inventory,
              product.price,
              category.name,
              product.image,
              product.link,
              product.codes,
              null,
              side,
          ]

          const query = `
              insert into parts values 
              (${range(1, values.length + 1).map(x => '$' + x)})
          `

          if (!dev) await pg.query(query, values)
          else console.log(query, values)

          console.log(`[${make.name}]`, 'Inserted:', product.title)
        }

        break
    }
  }), { parallelize: 5 })
}

(async () => {
  const _pg = new Postgres({ database: 'partsfit' })
  await _pg.connect()

  // const create_q = `
  //   create table if not exists parts (
  //     vendor text,
  //     id text,
  //     title text,
  //     inventory int,
  //     price numeric,
  //     category text,
  //     image text,
  //     link text,
  //     codes text,
  //     manufacturer text,
  //     side text
  //   )
  // `

  // const clear_q = `delete from parts where vendor = 'AL'`

  pg = _pg

  if (!dev) {
    await pg.query('delete from parts where vendor_id = $1', ['AL'])
  }

  let categories
  try {
    categories = await get_categories()
    if (dev) {
      categories = sample(categories)
      // categories = categories.filter(x => x.name === 'Defensas Delanteras')
      console.log('Dev ENV, chosing random category:', categories[0].name)
    }
  } catch (e) {
    console.log('There was an error scrapping Categories:')
    console.log(e)
  }

  await serial(categories.map(c => () => get_parts(c)))
})().catch(console.log)