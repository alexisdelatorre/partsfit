const Postgres = require('pg').Client
const sample = require('lodash.samplesize')
const parse_csv = require('d3-dsv').csvParse
const read_file = require('fs').readFileSync
const price = require('../../utils/price')
const { render: render_template } = require('mustache')
const _mws = require('mws-simple')

const dev = process.env.NODE_ENV !== 'production'

const translate_side = side => {
  if (side === 'na') return ''
  else if (side === 'right') return 'derecho'
  else if (side === 'left') return 'izquierdo'
}

function toTitleCase(str) {
  return str.replace(
      /\w\S*/g,
      function(txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
  );
}

const title = ({category, side, cars}) => toTitleCase([
  category,
  translate_side(side),
  cars.map(car => `${car.model} ${car.yearinit}-${car.yearlast}`).join(', ')
].join(' ').replace(/  /g, ' '))

const reduce_cars = (acc, { make, model, yearinit, yearlast, part_id, ...v }) => {
  const car_hash = (make + model + yearinit + yearlast)
    .replace(/ /g, '')
    .toUpperCase()

  if (!acc[v.id]) {
    acc[v.id] = { ...v, cars: [] }
  }

  const car = { hash: car_hash, make, model, yearinit, yearlast }
  acc[v.id].cars.push(car)
  
  return acc
}

// const upcs = [
//   689528816814
// ]

const mws = _mws({
  accessKeyId: process.env.AMAZON_ACCESS_KEY,
  secretAccessKey: process.env.AMAZON_ACCESS_SECRET,
  merchantId: process.env.AMAZON_SELLER_ID,
  authToken: process.env.AMAZON_TOKEN,
})

const request = params => new Promise((resolve, reject) => {
  mws.request(params, function(e, result) {
    if (e) reject(e)
    else resolve(result)
  })
})

;
(async () => {
  const pg = new Postgres({ database: 'partsfit' })
  await pg.connect()

  const categories = await parse_csv(read_file('data/categories.csv', 'utf-8'))

  const upcs = read_file('data/upc.txt', 'utf-8').split('\n')

  const parts_q = `
    select * from parts
    join cars on parts.id = cars.part_id
    where parts.vendor_id = $1
  `

  let parts

  parts = await pg.query(parts_q, ['AL'])
  parts = parts.rows
  parts = parts.reduce(reduce_cars, {})
  parts = Object.values(parts)

  // parts = sample(parts)

  parts = parts.map(part => {
    const part_cat = part.category.toLowerCase()

    const category = categories
      .filter(x => x.vendor = 'aldo')
      .filter(x => x.category.toLowerCase() === part_cat)
      [0]

    return { ...part, category }
  })

  parts = parts.filter(x => x.category)

  parts = parts.filter(x => x.category.ignore === '0')

  parts = parts.map(p => ({ ...p, category: p.category.title }))

  // parts = parts.filter(p => p.cars.length > 1)

  parts = sample(parts, upcs.length)
  // parts = sample(parts, 1)
  // parts = [parts[0]]

  // console.log(parts)

  parts = parts.map((part, i) => {
    const category = part.category

    const cars = part.cars

    const desc_data = {
      // seller_id: process.env.AMAZON_SELLER_ID,
      i: i + 1,
      id: part.id,
      title: title({ category: category, side: part.side, cars }),
      description: part.title,
      category: category,
      image: part.image,
      cars: cars.map(car => `${car.model} ${car.yearinit}-${car.yearlast}`).join(', '),
      side: translate_side(part.side),
      price: price({
        vendorPrice: Number(part.price),
        vendorDiscount: 0.325,
        tax: 0.16,
        shipping : 180,
        // commision : 0.12,
        shippingDiscount : 0,
        // more_than_550 : 0,
        expectedProfit : 0.10,
        isPremium: false,
        me: false,
      }),
      inventory: part.inventory,
      upc: upcs[i],
    }

    return desc_data
  })

  const seller_id = process.env.AMAZON_SELLER_ID

  const desc_template = read_file('utils/amz_templates/description-amazon.mustache', 'utf-8')
  const desc_feed = render_template(desc_template, { seller_id, parts })

  const desc_opts = {
    path: '/Feeds/2009-01-01',
    feedContent: desc_feed,
    query: {
      Action: 'SubmitFeed',
      FeedType: '_POST_PRODUCT_DATA_',
      'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
      MWSAuthToken: process.env.AMAZON_TOKEN,
    }
  }

  if (!dev) {
    let feed_sub_id
    feed_sub_id = await request(desc_opts)
    feed_sub_id = feed_sub_id.SubmitFeedResponse
    feed_sub_id = feed_sub_id.SubmitFeedResult[0]
    feed_sub_id = feed_sub_id.FeedSubmissionInfo[0]
    feed_sub_id = feed_sub_id.FeedSubmissionId[0]

    await new Promise((resolve, reject) => {
      console.log('processing feed submition:', feed_sub_id)

      const timer = setInterval(() => {
        request({
          path: '/Feeds/2009-01-01',
          query: {
            Action: 'GetFeedSubmissionResult',
            FeedSubmissionId: feed_sub_id,
            MWSAuthToken: process.env.AMAZON_TOKEN,
            Marketplace: 'A1AM78C64UM0Y8',
          }
        }).then(res => {
          if (res.AmazonEnvelope) {
            let status = res.AmazonEnvelope.Message[0].ProcessingReport[0].StatusCode[0]
            console.log(`status for ${feed_sub_id}:`, status)
            console.log(JSON.stringify(res, null, 2))
            clearInterval(timer)
            resolve()
          }
          else {
            console.log(`status for ${feed_sub_id}:`, 'Not Ready')
          }
        })
      }, 45000)
    })
  }
  else {
    console.log('===========================')
    console.log('QUERY:')
    console.log(desc_opts.query)
    console.log()
    console.log('FEED:')
    console.log(desc_feed)
    console.log('===========================')
  }

  const inventory_template = read_file('utils/amz_templates/inventory-amazon.mustache', 'utf-8')
  const inventory_feed = render_template(inventory_template, { seller_id, parts })

  const inventory_opts = {
    path: '/Feeds/2009-01-01',
    feedContent: inventory_feed,
    query: {
      Action: 'SubmitFeed',
      FeedType: '_POST_INVENTORY_AVAILABILITY_DATA_',
      'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
    }
  }
  
  if (!dev) {
    let inv_feed_id
    inv_feed_id = await request(inventory_opts)
    inv_feed_id = inv_feed_id.SubmitFeedResponse
    inv_feed_id = inv_feed_id.SubmitFeedResult[0]
    inv_feed_id = inv_feed_id.FeedSubmissionInfo[0]
    inv_feed_id = inv_feed_id.FeedSubmissionId[0]
    console.log('feed id for inventory:', inv_feed_id)
  }
  else {
    console.log('===========================')
    console.log('QUERY:')
    console.log(inventory_opts.query)
    console.log()
    console.log('FEED:')
    console.log(inventory_feed)
    console.log('===========================')
  }

  const price_template = read_file('utils/amz_templates/price-amazon.mustache', 'utf-8')
  const price_feed = render_template(price_template, { seller_id, parts })

  const price_opts = {
    path: '/Feeds/2009-01-01',
    feedContent: price_feed,
    query: {
      Action: 'SubmitFeed',
      FeedType: '_POST_PRODUCT_PRICING_DATA_',
      'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
    }
  }
  
  if (!dev) {
    let price_feed_id
    price_feed_id = await request(price_opts)
    price_feed_id = price_feed_id.SubmitFeedResponse
    price_feed_id = price_feed_id.SubmitFeedResult[0]
    price_feed_id = price_feed_id.FeedSubmissionInfo[0]
    price_feed_id = price_feed_id.FeedSubmissionId[0]
    console.log('feed id for price:', price_feed_id)
  }
  else {
    console.log('===========================')
    console.log('QUERY:')
    console.log(price_opts.query)
    console.log()
    console.log('FEED:')
    console.log(price_feed)
    console.log('===========================')
  }

  const img_template = read_file('utils/amz_templates/image-amazon.mustache', 'utf-8')
  const img_feed = render_template(img_template, { seller_id, parts })

  const img_opts = {
    path: '/Feeds/2009-01-01',
    feedContent: img_feed,
    query: {
      Action: 'SubmitFeed',
      FeedType: '_POST_PRODUCT_IMAGE_DATA_',
      'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
    }
  }
  
  if (!dev) {
    // const re = await request(img_opts)
    // console.log(JSON.stringify(re, null, 4))
    let img_feed_id
    img_feed_id = await request(img_opts)
    img_feed_id = img_feed_id.SubmitFeedResponse
    img_feed_id = img_feed_id.SubmitFeedResult[0]
    img_feed_id = img_feed_id.FeedSubmissionInfo[0]
    img_feed_id = img_feed_id.FeedSubmissionId[0]
    console.log('feed id for img:', img_feed_id)
  }
  else {
    console.log('===========================')
    console.log('QUERY:')
    console.log(img_opts.query)
    console.log()
    console.log('FEED:')
    console.log(img_feed)
    console.log('===========================')
  }

  console.log('done')
})()