const _mws = require('mws-simple')
const { Client: _pg } = require('pg')
const parallel = require('../../utils/parallel')

const dev = process.env.NODE_ENV !== 'production'

const mws = _mws({
    accessKeyId: process.env.AMAZON_ACCESS_KEY,
    secretAccessKey: process.env.AMAZON_ACCESS_SECRET,
    merchantId: process.env.AMAZON_SELLER_ID,
    authToken: process.env.AMAZON_TOKEN,
});

const request = params => new Promise((resolve, reject) => {
  mws.request(params, (e, result) => {
    if (e) reject(e)
    else resolve(result)
  })
})

;
(async () => {
  const pg = new _pg({ database: 'partsfit' })
  await pg.connect()

  const create_q = `
    create table if not exists posts_amz (
      asin text,
      part_id text,
      inventory int,
      price numeric
    )
  `

  const delete_q = `delete from posts_amz`  
  
  if (!dev) {
    await pg.query(create_q).catch(() => {})
    await pg.query(delete_q)
  }
  else {
    console.log(delete_q)
    console.log(create_q)
  }

  // step 1: request an open listings report
  const request_report = {
    path: '/Reports/2009-01-01',
    query: {
      Action: 'RequestReport',
      MWSAuthToken: process.env.AMAZON_TOKEN,
      Marketplace : 'A1AM78C64UM0Y8',
      ReportType : '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_',
    }
  }

  let report_request_id
  report_request_id = await request(request_report)
  report_request_id = report_request_id.RequestReportResponse
  report_request_id = report_request_id.RequestReportResult[0]
  report_request_id = report_request_id.ReportRequestInfo[0]
  report_request_id = report_request_id.ReportRequestId[0]

  console.log(report_request_id)

  // step 2: Pool the GetReportRequestList endpoint using the
  // ReportRequestId from step 1 waiting for a done status
  const report_id = await new Promise((resolve, reject) => {
      let report_list = id => ({
        path: '/Reports/2009-01-01',
        query: {
          Action: 'GetReportRequestList',
          MWSAuthToken: process.env.AMAZON_TOKEN,
          Marketplace : 'A1AM78C64UM0Y8',
          'ReportRequestIdList.Id.1': id,
        }
      })

      console.log('Checking report request:', report_request_id)

      const timer = setInterval(() => {
      request(report_list(report_request_id)).then(res => {
        if (Object.keys(res).indexOf('ErrorResponse') > -1) {
          clearInterval(timer)
          reject(JSON.stringify(res, null, 4))
        }
        else {
          let status
          status = res.GetReportRequestListResponse
          status = status.GetReportRequestListResult[0]
          status = status.ReportRequestInfo[0]
          status = status.ReportProcessingStatus[0]

          console.log(`status for ${report_request_id}:`, status)
  
          if (status === '_DONE_') {
            let report_id
            report_id = res.GetReportRequestListResponse
            report_id = report_id.GetReportRequestListResult[0]
            report_id = report_id.ReportRequestInfo[0]
            report_id = report_id.GeneratedReportId[0]
  
            clearInterval(timer)
            resolve(report_id)
          }
        }
      })
    }, 15000)
  })

  // step 3: use the GeneratedReportID from step 2 on the getReport endpoint
  let get_report = {
    path: '/Reports/2009-01-01',
    query: {
      Action: 'GetReport',
      ReportId: report_id,
    }
  }

  let items
  items = await request(get_report)
  // else items = [
  //   { sku: '11-A114-01-2B',
  //     asin: 'B06Y5FQH8N',
  //     price: '784.42',
  //     quantity: '18' },
  //   { sku: 'pf-11-1483-B1-6B',
  //     asin: 'B07DHZDLSZ',
  //     price: '175.46',
  //     quantity: '60' },
  //   { sku: 'pf-19-C168-01-2B',
  //     asin: 'B07DJ1BGT7',
  //     price: '1586.96',
  //     quantity: '10' },
  // ]

  // console.log(items)

  const values = items.map(x => [ x.asin, x.sku.replace('pf-', ''), x.quantity, x.price ])

  if (!dev) {
    try {
      await pg.query('begin')
      const insert_q = 'insert into posts_amz values ($1, $2, $3, $4)'
      parallel(values.map(v => () => pg.query(insert_q, v)), 20)
      await pg.query('commit')
    } catch (e) {
      console.log(e)
      await pg.query('rollback')
    }
  }
  else {
    const insert_q = 'insert into posts_amz values ($1, $2, $3, $4)'
    console.log(insert_q, values)
  }

  // pg.end()
})()

// ;
// (async () => {
//   const pg = new _pg()
//   await pg.connect()

//   const mws = _mws({
//     accessKeyId: process.env.AMAZON_ACCESS_KEY,
//     secretAccessKey: process.env.AMAZON_ACCESS_SECRET,
//     merchantId: process.env.AMAZON_SELLER_ID,
//     authToken: process.env.AMAZON_TOKEN,
//   })

//   const request = params => new Promise((resolve, reject) => {
//     mws.request(params, function(e, result) {
//       if (e) reject(e)
//       else resolve(result)
//     })
//   })

//   const post = await pg.query('select * from posts_amz limit 1')
//     .then(x => x.rows[0])

//   const res = await request({
//     path: '/Products/2011-10-01',
//     query: {
//       Action: 'ListMatchingProducts',
//       Query: 'ELP30809ER',
//       // 'IdList.Id.1': '11-6294-00-1A',
//       // IdType: 'SellerSKU',
//       'MarketplaceId': 'A1AM78C64UM0Y8',
//     }
//   })

//   console.log(JSON.stringify(res))

//   await pg.end()
// })()

// 11-A114-01-2B
// 11-A114-01-2B