const Postgres = require('pg').Client
const parallel = require('../../utils/parallel')
const Mws = require('mws-simple')
const calcPrice = require('../../utils/price')
const { readFileSync: read_file } = require('fs')
const { render: render_template } = require('mustache')
const sample = require('lodash.samplesize')
const serial = require('promise-serial')

const dev = process.env.NODE_ENV !== 'production'

function parseSubmitFeedResponse(submitFeedResponse) {
  const id = submitFeedResponse
    .SubmitFeedResponse
    .SubmitFeedResult[0]
    .FeedSubmissionInfo[0]
    .FeedSubmissionId[0]

  return { id, raw: JSON.stringify(submitFeedResponse) }
}

function parseFeedProcessingResult(feedProcessingResult) {
  let errorCode = null
  let statusCode = null
  let messages_proccessed = 0
  let messages_successful = 0
  let messages_error = 0
  let messages_warning = 0

  if (feedProcessingResult.ErrorResponse) {
    errorCode = feedProcessingResult
      .ErrorResponse
      .Error[0]
      .Code[0]
  }
  else {
    statusCode = feedProcessingResult
      .AmazonEnvelope
      .Message[0]
      .ProcessingReport[0]
      .StatusCode[0]

    const summary = feedProcessingResult
      .AmazonEnvelope
      .Message[0]
      .ProcessingReport[0]
      .ProcessingSummary[0]

    messages_proccessed = summary.MessagesProcessed[0]
    messages_successful = summary.MessagesSuccessful[0]
    messages_error = summary.MessagesWithError[0]
    messages_warning = summary.MessagesWithWarning[0]
  }

  return {
    errorCode, statusCode,
    messages_proccessed,
    messages_successful,
    messages_error,
    messages_warning,
    raw: JSON.stringify(feedProcessingResult),
  }
}

function Client() {
  this.pg = new Postgres({ database: 'partsfit' })

  this.mws = Mws({
    accessKeyId: process.env.AMAZON_ACCESS_KEY,
    secretAccessKey: process.env.AMAZON_ACCESS_SECRET,
    merchantId: process.env.AMAZON_SELLER_ID,
    authToken: process.env.AMAZON_TOKEN,
  })

  this.request = params => new Promise((resolve, reject) => {
    this.mws.request(params, function(e, result) {
      if (e) reject(e)
      else resolve(result)
    })
  })
}

function request(params) {
  new Promise((resolve, reject) => {
    this.mws.request(params, function(e, result) {
      if (e) reject(e)
      else resolve(result)
    })
  })
}

Client.prototype.connect = async function() {
  await this.pg.connect()
}

Client.prototype.getPosts = async function() {
  let posts
  posts = await this.pg.query('select * from posts_amz').then(x => x.rows)
  if (dev) posts = sample(posts, 5)
  posts = posts.map(x => x.asin)

  return posts
}

Client.prototype.getChanges = async function(post_id) {
  let price = null
  let inventory = null

  const post = await this.pg.query('select * from posts_amz where asin = $1', [post_id])
    .then(x => x.rows[0])

  const part = await this.pg.query('select * from parts where id = $1', [post.part_id])
    .then(x => x.rows[0])

  if (part === undefined) {
    console.log(`[${post_id}] No part`)
    return { id: '', price: null, inventory: null }
  }

  let newPrice
  newPrice = calcPrice({
    price : Number(part.price),
    discount : 0.325,
    tax : 0.16,
    shipping : 80,
    commision : 0.12,
    less_than_550 : 0,
    more_than_550 : 0,
    expectedProfit : 0.05,
    me: false,
  })
  if(Number(post.price) === newPrice) newPrice = null

  let newInventory
  newInventory = part.inventory
  if (newInventory === post.inventory) newInventory = null

  let msgs = []

  if (newPrice !== null) msgs.push(`price: ${post.price} -> ${newPrice}`)
  if (newInventory !== null) msgs.push(`inventory: ${post.inventory} -> ${newInventory}`)

  if (msgs.length > 0) console.log(`[${post_id}] ${msgs.join(', ')}`)
  else console.log(`[${post_id}] No changes`)

  return { id: part.id, price: newPrice, inventory: newInventory }
}

Client.prototype.applyChanges = async function(changes) {
  let pricesSubmitFeedId
  let invSubmitFeedId

  const prices = changes
    .filter(x => x.price !== null)
    .map(({ id, price }, i) => ({ id, price, i: i + 1 }))

  if (prices.length > 0) {
    console.log(`Making requests to update ${prices.length} prices`)

    const price_data = {
      seller_id: process.env.AMAZON_SELLER_ID,
      parts: prices,
    }
    const price_template = read_file('utils/amz_templates/price-amazon.mustache', 'utf-8')
    const price_feed = render_template(price_template, price_data)
  
    const price_opts = {
      path: '/Feeds/2009-01-01',
      feedContent: price_feed,
      query: {
        Action: 'SubmitFeed',
        FeedType: '_POST_PRODUCT_PRICING_DATA_',
        'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
      }
    }

    const res = await this.request(price_opts)

    pricesSubmitFeedId = parseSubmitFeedResponse(res).id
  
    const updatePriceQ = 'update posts_amz set price = $1 where part_id = $2'
  
    await serial(prices.map(p => () => this.pg.query(updatePriceQ, [p.price, p.id])))

    let parsedPricesFeedProcessingResult

    while (!parsedPricesFeedProcessingResult || parsedPricesFeedProcessingResult.errorCode !== null) {
      const res = await this.request({
        path: '/Feeds/2009-01-01',
        query: {
          Action: 'GetFeedSubmissionResult',
          FeedSubmissionId: pricesSubmitFeedId,
          MWSAuthToken: process.env.AMAZON_TOKEN,
          Marketplace: 'A1AM78C64UM0Y8',
        }
      })
  
      parsedPricesFeedProcessingResult = parseFeedProcessingResult(res)
      console.log(parsedPricesFeedProcessingResult)
  
      // sleep for 1 minute
      await new Promise(res => setTimeout(res, 60000))
    }
  
    console.log('Success!', 'Prices')
    console.log(parsedPricesFeedProcessingResult)
  }
  
  const inventories = changes
    .filter(x => x.inventory !== null)
    .map(({ id, inventory }, i) => ({ id, inventory, i: i + 1 }))

  if (inventories.length > 0) {
    console.log(`Making requests to update ${inventories.length} inventories`)

    const inventories_data = {
      seller_id: process.env.AMAZON_SELLER_ID,
      parts: inventories
    }
    const inventories_template = read_file('utils/amz_templates/inventory-amazon.mustache', 'utf-8')
    const inventories_feed = render_template(inventories_template, inventories_data)
  
    const inventories_opts = {
      path: '/Feeds/2009-01-01',
      feedContent: inventories_feed,
      query: {
        Action: 'SubmitFeed',
        FeedType: '_POST_INVENTORY_AVAILABILITY_DATA_',
        'MarketplaceIdList.Id': 'A1AM78C64UM0Y8',
      }
    }

    const res = await this.request(inventories_opts)
    invSubmitFeedId = parseSubmitFeedResponse(res).id
  
    const updateInvQ = 'update posts_amz set inventory = $1 where part_id = $2'
  
    await serial(inventories.map(p => () => this.pg.query(updateInvQ, [p.inventory, p.id])))

    let parsedInvFeedProcessingResult
    while (!parsedInvFeedProcessingResult || parsedInvFeedProcessingResult.errorCode !== null) {
      const res = await this.request({
        path: '/Feeds/2009-01-01',
        query: {
          Action: 'GetFeedSubmissionResult',
          FeedSubmissionId: invSubmitFeedId,
          MWSAuthToken: process.env.AMAZON_TOKEN,
          Marketplace: 'A1AM78C64UM0Y8',
        }
      })

      parsedInvFeedProcessingResult = parseFeedProcessingResult(res)
      console.log(parsedInvFeedProcessingResult)

      // sleep for 1 minute
      await new Promise((res) => setTimeout(res, 60000))
    }

    console.log('Success!', 'Inventory')
    console.log(parsedInvFeedProcessingResult)
  }
}

;
(async () => {
  const client = new Client()
  await client.connect()

  const posts = await client.getPosts()

  console.log('Computing changes')

  const changes = await serial(posts.map(p => () => client.getChanges(p)))
    .then(c => c.filter(x => x.price !== null || x.inventory !== null))

  await client.applyChanges(changes)
})()