const Postgres = require('pg').Client
const sample = require('lodash.samplesize')
const serial = require('promise-serial')

const dev = process.env.NODE_ENV !== 'production'

const account = ''

;
(async function () {
    const pg = new Postgres()
    await pg.connect()

    let posts
    posts = await pg.query('select * from posts')
    posts = posts.rows
    if (dev) posts = sample(posts, 1000)

    let noPart = 0

    let parts
    parts = await pg.query('select * from parts')
        .then(x => x.rows)

    let allCategories = []
    for (let i = 0; i < parts.length; i++) {
        const p = parts[i]
        if (allCategories.map(x => x.name).includes(p.category)) continue
        else allCategories.push({ name: p.category, vendor: p.vendor })
    }

    let accounts = []
    for (let i = 0; i < posts.length; i++) {
        const p = posts[i]
        if (accounts.includes(p.account)) continue
        else accounts.push(p.account)
    }

    let categories = {}
    for (let i = 0; i < accounts.length; i++) {
        const ac = accounts[i]
        let hashes = []
        for (let i = 0; i < allCategories.length; i++) {
            const cat = allCategories[i]

            const hash = `${cat.name} || ${cat.vendor} || ${ac}`
                .toUpperCase()

            categories[hash] = {}

            categories[hash].category = cat.name
            categories[hash].vendor = cat.vendor
            categories[hash].account = ac

            categories[hash].all = []
            categories[hash].active = []
            categories[hash].paused = []
            categories[hash].other = []
            categories[hash].unique = []
        }
    }

    let count = 0
    for (let i = 0; i < posts.length; i++) {
        const post = posts[i]

        const part = await pg.query('select * from parts where id = $1', [post.part_id])
            .then(x => x.rows[0])

        if (!part) {
            noPart++
            count++
            console.log(`${count}/${posts.length}`)
            continue
        }

        const hash = `${part.category} || ${part.vendor} || ${post.account}`.toUpperCase()

        // if (!categories[hash]) {
        //     categories[hash] = {}

        //     categories[hash].category = part.category
        //     categories[hash].vendor = part.vendor
        //     categories[hash].account = post.account

        //     categories[hash].all = []
        //     categories[hash].active = []
        //     categories[hash].paused = []
        //     categories[hash].other = []
        //     categories[hash].unique = []
        // }

        // console.log(categories[hash])

        categories[hash].all.push({ post: post.id, part: part.id })

        // console.log(hash)

        if(!categories[hash].unique.includes(part.id)) {
            categories[hash].unique.push(part.id)
        }

        if (post.status === 'active') categories[hash].active.push(part.id)
        else if (post.status === 'paused' || Number(post.inventory) === 0) {
            categories[hash].paused.push(part.id)
        }
        else categories[hash].other.push(part.id)

        count++
        console.log(`${count}/${posts.length}`)
    }

    categories_an = []

    for (let i = 0; i < Object.values(categories).length; i++) {
        const category = Object.values(categories)[i]

        const q = 'select * from parts where category = $1'
        const part_qty = await pg.query(q, [category.category])
            .then(x => x.rows.length)

        categories_an.push({
            ...category,
            part_qty,
            all_qty: category.all.length,
            active_qty: category.active.length,
            paused_qty: category.paused.length,
            other_qty: category.other.length,
            unique_qty: category.unique.length,
        })
    }

    let csv = ''
    csv += 'category,vendor,account,all,active,paused,other,unique,vendor'
    for (let i = 0; i < categories_an.length; i++) {
        const c = categories_an[i]
        const values = [
            c.category.replace(/,/g, ''),
            c.vendor,
            c.account,
            c.all_qty,
            c.active_qty,
            c.paused_qty,
            c.other_qty,
            c.unique_qty,
            c.part_qty,
        ]
        csv += '\n' + values.join(',')
    }
    require('fs').writeFileSync('export/posts-vs-parts.csv', csv)
})()