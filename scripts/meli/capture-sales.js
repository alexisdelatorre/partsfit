const { readFileSync: read_file } = require('fs')
const { get } = require('axios')
const get_token = require('meli-auth')
const sqlite = require('sqlite')

const get_shipping_price = require('../src/shipping-price')
const create_gsheet = require('../src/google-sheets')
const parallel = require('../src/parallel')

let creds
creds = read_file('data/google-credentials.json')
creds = creds.toString()
creds = JSON.parse(creds)

const sheet_id = '1kFrEgaXL9edX7p7zn4IPQ1kShf00jJbI2rcZDP67Kx8'

const translate_side = side => {
  if (side === 'right') return 'Derecho'
  else if (side === 'left') return 'Izquierdo'
  else return 'N/A'
}

let cache = {}
let row_count = 0

const run = async (env, order_id) => {
  const { host } = env
  const { token } = env
  const { seller_id } = env
  const { sqlite_processed } = env
  const { sqlite_internal } = env
  const { categories } = env
  const { account } = env
  const { gsheet } = env
  const { worksheet_id } = env
  const { last_row } = env

  // order

  const link = [
    host,
    '/orders',
    '/search',
    `?seller=${seller_id}`,
    `&q=${order_id}`,
    `&access_token=${token}`,
  ].join('')

  let order
  order = await get(link)
  if (order.data.results.length > 1) console.log('multiple results, ', order_id)  
  order = order.data.results[0]

  // item

  let item
  item = order.order_items
  if (item.length > 1) console.log('multiple items, ', order_id)
  item = item[0]

  // post

  const post_q = 'select * from posts where id = ?'

  let post
  post = await sqlite_processed.all(post_q, item.item.id)
  post = post[0]

  // part

  const parts_id = post.part_id.split(' & ')

  const parts_q =  `select * from parts where id in (${parts_id.map(() => '?').join(', ')})`

  let parts = await sqlite_processed.all(parts_q, parts_id)

  // vendor

  const vendor_q = 'select * from vendors where vendor_id = ?'

  let vendor
  vendor = await sqlite_internal.get(vendor_q, parts[0].vendor)

  // category

  const category =
  categories
  .filter(c => c.vendor === vendor.vendor_name)
  .filter(c => c.category_meli === post.category)
  [0]

  // shipping

  const shipping_type = category.shipping_type

  let shipping_price

  if (category.ignore === '1') {
    shipping_price = 'ERROR: CATEGORIA IGNORADA'
  }
  else if (cache[category.category_meli]) {
    shipping_price = parseInt(cache[category.category_meli])
  }
  else if (shipping_type === 'ME') {
    shipping_price = await get_shipping_price(post.category)

    if (order.total_amount > 550) {
      shipping_price = shipping_price * (1 - account.more_than_550)
    }
    else {
      shipping_price = shipping_price * (1 - account.less_than_550)      
    }

    cache[category.category_meli] = parseInt(shipping_price)
  }
  else {
    shipping_price = parseInt(shipping_type.split(':')[1])
  }

  // capture

  add_rows = parts.map((part, idx) => async () => {
    row_count += 1

    // current row
    const cr = last_row + row_count

    let row = {}
    row['operacion'] = order_id
    row['fechadecompra'] = order.date_closed // format this
    row['fechadeenvio'] = Date.now() // format this
    row['supplier'] = vendor.vendor_name.toUpperCase()
    row['publicacion'] = post.premium === '1' ? 'PR' : 'CL'
    row['pfid'] = 'PF-###'
    row['cnt'] = 1
    row['lados'] = translate_side(part.side)
    row['idparte'] = part.id
    row['descripcion'] = part.title
    row['link'] = part.link
    row['link2'] = 'x'
    row['guia'] = ''
    row['paqueteria'] = ''
    row['originalu'] = parts.map(p => p.price).reduce((p1, p2) => p1 + p2)
    row['compra'] = `=IF(O${cr}="","",(IFS(D${cr}="ALDO" , ((O${cr}*0.675)*1.16) , D${cr}="OPTIMO" ,(O${cr}*1)*1.16))*G${cr})`
    row['venta'] = order.total_amount
    row['envio'] = shipping_price
    row['gananciabruta'] = `=IF(O${cr}="","",(Q${cr}-P${cr}))`
    row['comisionml'] = `=IF(O${cr}="","",(IFS((Q${cr}/G${cr})>=5000,IFS(E${cr}="CL",(((Q${cr}/G${cr})*0.07)+180),E${cr}="PR",(((Q${cr}/G${cr})*0.115)+180)),(Q${cr}/G${cr})>=1000,IFS(E${cr}="CL",(((Q${cr}/G${cr})*0.1)+30),E${cr}="PR",(((Q${cr}/G${cr})*0.145)+30)),(Q${cr}/G${cr})<1000,IFS(E${cr}="CL",((Q${cr}/G${cr})*0.13),E${cr}="PR",((Q${cr}/G${cr})*0.175))))*G${cr})`
    row['ganancianeta'] = `=IF(O${cr}="","",(IFS((Q${cr})>=5000,IFS(E${cr}="CL",S${cr}-R${cr}-(((Q${cr})*0.07)+180),E${cr}="PR",S${cr}-R${cr}-(((Q${cr})*0.115)+180)),(Q${cr})>=1000,IFS(E${cr}="CL",S${cr}-R${cr}-(((Q${cr})*0.1)+30),E${cr}="PR",S${cr}-R${cr}-(((Q${cr})*0.145)+30)),(Q${cr})<1000,IFS(E${cr}="CL",S${cr}-R${cr}-((Q${cr})*0.13),E${cr}="PR",S${cr}-R${cr}-((Q${cr})*0.175)))))`
    row['ganancia'] = `=IF(O${cr}="","",(U${cr}/P${cr}))`
    row['notas'] = ''
    row['accion'] = ''

    if (idx !== 0) {
      row['originalu'] = '^'
      row['compra'] = '^'
      row['venta'] = '^'
      row['envio'] = '^'
      row['gananciabruta'] = '^'
      row['comisionml'] = '^'
      row['ganancianeta'] = '^'
      row['ganancia'] = '^'
    }

    await gsheet.add_row(worksheet_id, row)

    console.log(`[${order_id}] ${part.title}\n`)
  })

  parallel(add_rows, 1)
}

const init = async () => {
  const sqlite_processed = await sqlite.open('data/processed.sqlite')
  const sqlite_internal = await sqlite.open('data/internal.sqlite')

  const categories = await sqlite_internal.all('select * from categories')

  const gsheet = await create_gsheet(sheet_id, creds)

  const worksheet_name = 'Sheet1'

  let worksheet
  worksheet = await gsheet.get_info()
  worksheet = worksheet.worksheets
  worksheet = worksheet.filter(x => x.title === worksheet_name)[0]

  const worksheet_id = worksheet.id

  let last_row
  last_row = await gsheet.get_rows(worksheet_id)
  last_row = last_row.length + 1

  let last_captured
  last_captured = await gsheet.get_rows(worksheet_id)
  last_captured = last_captured.map(r => r.operacion)
  last_captured = last_captured.reverse()[0]
  last_captured = parseInt(last_captured)

  const last_captured = 1701062501 // development

  const token = await get_token({}).promise()

  const host = 'https://api.mercadolibre.com'

  let seller_id
  seller_id = await get(`${host}/users/me?access_token=${token}`)
  seller_id = seller_id.data.id

  const account_q = 'select * from accounts where id = ?'
  const account = await sqlite_internal.get(account_q, seller_id)

  let orders = []

  let offset = 0
  const limit = 50
  const rec = async () => {
    const link = [
      host,
      '/orders',
      '/search',
      `?seller=${seller_id}`,
      `&access_token=${token}`,
      `&sort=date_desc`,
      `&offset=${offset}`,
      `&limit=${limit}`
    ].join('')

    let orders_chunk
    orders_chunk = await get(link)
    orders_chunk = orders_chunk.data

    let orders_id

    orders_id = orders_chunk.results.map(x => x.id)

    const index = orders_id.indexOf(last_captured)

    if (index !== -1) orders_id = orders_id.slice(0, index)

    offset += limit
    if (index !== -1) orders = orders.concat(orders_id)
    else await rec()
  }
  await rec()

  const env = {
    host,
    token,
    seller_id,
    sqlite_processed,
    sqlite_internal,
    categories,
    account,
    gsheet,
    worksheet_id,
    last_row,
  }

  const orders = [1701034158]
  
  parallel(orders.reverse().map(order_id => () => run(env, order_id)), 1)
}

init()