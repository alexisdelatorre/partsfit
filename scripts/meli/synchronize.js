const get_token = require('meli-auth')
const { get, put } = require('axios')
const sample = require('lodash.samplesize')
const R = require('ramda')
const parallel = require('../../utils/parallel')
const min = require('lodash.min')
const chunk = require('lodash.chunk')
const { Client: _pg } = require('pg')
const { csvParse: parse_csv } = require('d3-dsv')
const { readFileSync: read_file } = require('fs')
const aws = require('aws-sdk')
const { writeFileSync: write_file } = require('fs')

const dev = process.env.NODE_ENV !== 'production'

const host = 'https://api.mercadolibre.com'

aws.config.update({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET,
  endpoint: process.env.S3_ENDPOINT,
  s3ForcePathStyle: true,
  signatureVersion: 'v4',
})
const s3 = new aws.S3()

const parse_desc = require('../../utils/parse-description')
const get_shipping_price = require('../../utils/shipping-price')
const generate_desc = require('../../utils/description')
const calc_price = require('../../utils/price')

const get_parts = async (pg, post_id) => {
  // console.log(post_id)
  const query = 'select * from parts where id = any ($1)'
  
  let parts
  parts = await pg.query(query, [post_id.split(' & ')])
  parts = parts.rows

  if (parts.length !== post_id.split(' & ').length) {
    return []
  }
  else {
    return parts
  }
}

const trim_desc = desc => desc.split('\n').map(x => x.trim()).join('\n')

const log = (id, _message) => {
  count += 1
  
  const percent =
  x => (x*100).toFixed(2)

  const message = [
    `[${count}/${total}]`,
    `[${percent(count/total)}%]`,
    `[${id}]`,
    `${_message}\n`,
  ]

  console.log(message.join(' '))

  already_logged = true
}

const cache = {}

const run = async (env, _post) => {
  const pause_item = async () => {
    if (post.inventory === 0) return

    const link = `${host}/items/${post.id}?access_token=${token}`
    const query = 'update posts set inventory = 0 where id = $1'
    
    if (!dev) {
      try {
        await put(link, { available_quantity: 0 })
      } catch (error) {
        return
      }
      await pg.query(query, post.id).catch(() => {})
    }
    else {
      console.log('===')
      console.log(`link: ${link}`)
      console.log(`body: ${JSON.stringify({ available_quantity: 0 })}`)
      console.log('===')
      console.log(`query: ${query}`)
      console.log(`values: ${post.id}`)
    }
  }

  const post = R.evolve({ price: parseFloat }, _post)

  const { pg, token, vendors, categories, account } = env

  if (env.exceptions.indexOf(post.id) !== -1) {
    log(post.id, 'this post is in the exceptions file')
    return
  }

  if (post.description_html !== '') {
    log(post.id, 'html description')
    await pause_item()
    return
  }

  if (post.description_text === null) {
    log(post.id, 'null description')
    await pause_item()
    return
  }

  if (post.status === 'under_review') {
    log(post.id, 'under review')
    return
  }

  // metadata

  const { part_id, vendor_id, years, make, model } =
  parse_desc(post.description_text)

  if (part_id === undefined) {
    log(post.id, 'no part_id')
    await pause_item()
    return
  }

  if (vendor_id === undefined) {
    log(post.id, 'no vendor_id')
    await pause_item()
    return
  }

  if (years === undefined) {
    log(post.id, 'no years')
    await pause_item()
    return
  }

  if (make === undefined) {
    log(post.id, 'no make')
    await pause_item()
    return
  }

  if (model === undefined) {
    log(post.id, 'no model')
    await pause_item()
    return
  }

  // parts

  let parts
  parts = await get_parts(pg, part_id)
  // parts = parts.rows
  if (R.any(R.equals(undefined), parts) || parts.length === 0) {
    log(post.id, 'missing parts')
    await pause_item()
    return
  }

  // vendor & category

  const vendor =
  vendors
  .filter(R.propEq('vendor_id', vendor_id))
  [0]

  const category =
  categories
  .filter(R.propEq('vendor', vendor.vendor_name))
  .filter(R.propEq('category_meli', post.category))
  [0]

  // i have to get the category from the parts because the are a lot of posts with incorrect categories
  const part_category = categories
  .filter(R.propEq('vendor', vendor.vendor_name))
  .filter(x => x.category.toLowerCase() === parts[0].category.toLowerCase())
  [0]

  if (part_category === undefined || part_category.ignore === '1') {
    log(post.id, 'ignore category')
    await pause_item()
    return
  }

  if (vendor.accounts.indexOf(account.id) === -1) {
    log(post.id, 'the vendor is not registered in this account')
    await pause_item()
    return
  }

  // shipping

  const shipping_type = part_category.shipping_type

  let shipping_price
  let me

  if (cache[post.category]) {
    shipping_price = parseInt(cache[post.category]),
    me = true
  }
  else if (shipping_type === 'ME') {
    shipping_price = await get_shipping_price(post.category)
    me = true
    cache[post.category] = parseInt(shipping_price)
  }
  else {
    shipping_price = parseInt(shipping_type.split(':')[1]),
    me = false
  }

  // cars

  const isPair =
  parts.length === 2
  && R.any(R.equals(parts[0]), parts.map(R.prop('title')))

  const postCar = {
    make,
    model,
    yearinit: years.split('-')[0],
    yearlast: years.split('-')[1],
  }

  let cars = [ postCar ]

  const carsQuery = 'select * from cars where part_id = $1'

  if (parts.length === 1 || isPair) {
    cars = await pg.query(carsQuery, [parts[0].id])
    cars = cars.rows
  }

  if (cars.length === 0) {
    log(post.id, 'no cars')
    await pause_item()
    return
  }

  // rn

  let rn
  rn = post.description_text
  rn = rn.trim()
  rn = rn.split('\n')
  rn = rn[rn.length - 1]
  rn = rn.trim()
  rn = parseFloat(rn)
  rn = isNaN(rn) ? Math.random() * 5 : rn

  // sync

  let msg = []

  // console.log(priceNew({
  //   vendorPrice: 1000,
  //   vendorDiscount: 0,
  //   tax: 0,
  //   shipping: 0,
  //   expectedProfit: 0,
  //   shippingDiscount: 0,
  //   me: false,
  //   isPremium: true,
  // }))

  // price sync
  let new_price
  new_price = calc_price({
    vendorPrice: parts.map(x => x.price).map(parseFloat).reduce(R.add),
    vendorDiscount: parseFloat(vendor.discount),
    tax: 0.16,
    shipping: shipping_price,
    // commision : post.premium ? 0.175 : 0.13,
    shippingDiscount : parseFloat(account.less_than_550),
    // more_than_550 : parseFloat(account.more_than_550),
    expectedProfit: parseFloat(vendor.expected_profit),
    isPremium: post.premium,
    me,
  })
  new_price = new_price + rn
  new_price = new_price.toFixed(2)
  new_price = parseFloat(new_price)
  if (Math.max(new_price, post.price) - Math.min(new_price, post.price) < 5) {
    new_price = null
  }

  if (new_price != null) {
    const link = `${host}/items/${post.id}?access_token=${token}`

    if (!dev) {
      try {
        await put(link, { price: new_price })
      } catch (error) {
        log(post.id, 'error syncing price to meli')
        console.log(error)
        await pause_item()
        return
      }
    }
    else {
      console.log('===')
      console.log(`link: ${link}`)
      console.log(`body: ${JSON.stringify({ price: new_price })}`)
    }

    const update_q = 'update posts set price = $1 where id = $2'

    if (!dev) {
      await pg.query(update_q, [new_price, post.id])
    }
    else {
      console.log('===')
      console.log(`query: ${update_q}`)
      console.log(`values: ${[new_price, post.id]}`)
    }

    msg.push(`price (${post.price} -> ${new_price})`)
  }

  // inventory sync
  let new_inventory = min(parts.map(R.prop('inventory')))
  if (new_inventory < vendor.min_inventory) new_inventory = 0
  if (post.inventory === new_inventory) new_inventory = null

  if (new_inventory != null) {
    const link = `${host}/items/${post.id}?access_token=${token}`
    const body = { available_quantity: new_inventory }

    if (!dev) {
      try {
        await put(link, body)
      } catch (error) {
        log(post.id, 'error syncing inventory to meli')
        console.log(error)
        await pause_item()
        return
      }
    }
    else {
      console.log('===')
      console.log(`link: ${link}`)
      console.log(`body: ${JSON.stringify(body)}`)
    }

    const update_q = 'update posts set inventory = $1 where id = $2'
    const values = [new_inventory, post.id]

    if (!dev) {
      await pg.query(update_q, values)
    }
    else {
      console.log('===')
      console.log(`query: ${update_q}`)
      console.log(`values: ${values}`)
    }

    msg.push(`inventory (${post.inventory} -> ${new_inventory})`)
  }

  // // description sync
  // let new_description = generate_desc({
  //   business: account.title,
  //   make,
  //   model,
  //   category: parts.map(R.prop('category')).join(' & '),
  //   yearinit: years.split('-')[0],
  //   yearlast: years.split('-')[1],
  //   side: parts.length === 1 ? parts[0].side : parts.length === 2 ? 'Ambos' : 'Combo',
  //   id: parts.map(R.prop('id')).join(' & '),
  //   vendor_id,
  //   title: parts.map(R.prop('title')).map(R.toUpper).join(' & '),
  //   cars,
  //   random: rn,
  // })
  // if (trim_desc(post.description_text) === trim_desc(new_description)){
  //   new_description = null
  // }

  // if (new_description) {
  //   const link = `${host}/items/${post.id}/description?access_token=${token}`
  //   const body = { plain_text: new_description }

  //   if (!dev) {
  //     try {
  //       await put(link, body)
  //     } catch (error) {
  //       log(post.id, 'error syncing description to meli')
  //       console.log(error)
  //       await pause_item()
  //       return
  //     }
  //   }
  //   else {
  //     console.log('===')
  //     console.log(`link: ${link}`)
  //     console.log(`body: ${JSON.stringify(body)}`)
  //   }

  //   const update_q = 'update posts set description_text = $1 where id = $2'
  //   const values = [new_description, post.id]

  //   if (!dev) {
  //     await pg.query(update_q, values)
  //   }
  //   else {
  //     console.log('===')
  //     console.log(`query: ${update_q}`)
  //     console.log(`values: ${values}`)
  //   }

  //   msg.push('description')
  // }

  // // Sometimes ML cant process the part image, if so we will upload it
  // // to an s3 server and use that link.
  // // TODO: cache the post image on the scrapping
  // let post_image
  // try {
  //   post_image = await get(`${host}/items/${post.id}`)
  //   post_image = post_image.data.pictures[0].url
  // } catch (e) {
  //   post_image = ''
  // }

  // // change the image if it is still ML's processing stump
  // if (post_image.includes('proccesing')) {
  //   const parts_id = parts.map(x => x.id)
  //   const part_image = parts[0].image

  //   let img_buff
  //   img_buff = await get(part_image, { responseType: 'arraybuffer'})
  //   img_buff = img_buff.data

  //   await s3.putObject({
  //     Bucket: 'optimo-images',
  //     Key: `${post.id}.jpg`,
  //     Body: img_buff,
  //     ContentType: 'image/jpeg',
  //     ACL: 'public-read',
  //   }).promise()

  //   const s3_link = `${process.env.S3_ENDPOINT}/optimo-images/${post.id}.jpg`
  //   const link = `${host}/items/${post.id}?access_token=${token}`
  //   await put(link, { pictures: [{source: s3_link}] })

  //   msg.push('image')
  // }

  // if there where changes log them
  if (msg.length > 0) log(post.id, msg.join(', '))
  else log(post.id, 'no changes')
}

let total = 0
let count = 0;

(async () => {
  const token = await get_token({}).promise()
  
  let account_id
  account_id = await get(`${host}/users/me?access_token=${token}`)
  account_id = account_id.data.id
  
  const pg = new _pg()
  await pg.connect()

  let account_vendors
  account_vendors = await parse_csv(read_file('data/vendors_accounts.csv', 'utf-8'))

  let vendors
  vendors = await parse_csv(read_file('data/vendors.csv', 'utf-8'))
  vendors = vendors.map(v => {
    const accounts = account_vendors
      .filter(({ vendor_id }) => vendor_id === v.vendor_id)
      .map(x => x.account_id)
    return { ...v, accounts }
  })

  const categories = await parse_csv(read_file('data/categories.csv', 'utf-8'))

  let account
  account = await parse_csv(read_file('data/accounts.csv', 'utf-8'))
  account = account.filter(x => x.id === account_id.toString())[0]

  const posts_q = 'select * from posts where account = $1'

  let posts
  posts = await pg.query(posts_q, [account_id])
  posts = posts.rows
  // posts = posts.reverse()
  if (dev) posts = sample(posts, 1)

  const exceptions = read_file('data/exceptions.txt', 'utf-8')
    .split('\n')
    .map(x => 'MLM' + x)

  total = posts.length

  const env = { pg, token, vendors, categories, account, exceptions }
  
  await parallel(posts.reverse().map(post => () => run(env, post)), 20)
})()