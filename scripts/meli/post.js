const parallel = require('../../utils/parallel')
const { Client: _pg } = require('pg')
const sample = require('lodash.samplesize')
const { csvParse: parse_csv } = require('d3-dsv')
const { readFileSync: read_file } = require('fs')
const { get, post } = require('axios')
const get_token = require('meli-auth')
const get_shipping_price = require('../../utils/shipping-price')
const get_missing = require('../../utils/missing')
const gen_title = require('../../utils/title')
const gen_desc = require('../../utils/description')
const calc_price = require('../../utils/price')
const range = require('lodash.range')
const serial = require('promise-serial')
const min = require('lodash.min')
const groupby = require('lodash.groupby')
const cmd = require('commander')

const host = 'https://api.mercadolibre.com'

const dev = process.env.NODE_ENV !== 'production'

const cache = {}

const shipping_me = {
  mode: 'me2',
  local_pick_up: false,
  free_shipping: true,
  free_methods: [
    {
      id: 501245,
      rule: {
        free_mode: 'country',
        value: null
      }
    }
  ],
  dimensions: null,
  tags: ['me2_available'],
  logistic_type: 'drop_off',
  store_pick_up: false
}

const shipping = {
  "mode": "not_specified",
   "dimensions": null,
   "local_pick_up": false,
   "free_shipping": true,
   "logistic_type": "not_specified",
   "store_pick_up": false
}

// check if all elements of an array are equal
// https://stackoverflow.com/questions/14832603/check-if-all-values-of-array-are-equal
const allEquals = arr => arr.every( (val, i, self) => val === self[0] )

function permute(permutation) {
  var length = permutation.length,
      result = [permutation.slice()],
      c = new Array(length).fill(0),
      i = 1, k, p;

  while (i < length) {
    if (c[i] < i) {
      k = i % 2 && c[i];
      p = permutation[i];
      permutation[i] = permutation[k];
      permutation[k] = p;
      ++c[i];
      i = 1;
      result.push(permutation.slice());
    } else {
      c[i] = 0;
      ++i;
    }
  }
  return result;
}

const res = {}

function Client(pg) {
  this.pg = pg
}

Client.prototype.connect = async function () {
  const token = await get_token({}).promise()

  let account_id
  account_id = await get(`${host}/users/me?access_token=${token}`)
  account_id = account_id.data.id

  let account
  account = await parse_csv(read_file('data/accounts.csv', 'utf-8'))
  account = account.filter(x => x.id === account_id.toString())[0]
  account.less_than_550 = parseFloat(account.less_than_550)
  account.more_than_550 = parseFloat(account.more_than_550)

  let vendors
  vendors = await parse_csv(read_file('data/vendors.csv', 'utf-8'))
  // vendors = vendors.filter(x => Number(x.account_id) === account_id)

  const categories = await parse_csv(read_file('data/categories.csv', 'utf-8'))
  
  // const env = { pg, account, vendors, categories, token }

  this.account = account
  this.vendors = vendors
  this.categories = categories
  this.token = token
}

Client.prototype.getAllParts = async function () {
  const parts_q = 'select * from parts where vendor = any ($1)'
  
  let parts
  parts = await this.pg.query(parts_q, [this.vendors.map(x => x.vendor_id)])
  parts = parts.rows
  parts = parts.map(x => [x.id])
  if (dev) parts = sample(parts)

  this.total = parts.length

  return parts
}

Client.prototype.getAllPairs = async function () {
  const parts_q = 'select * from parts where vendor = any ($1)'
  
  let parts
  parts = await this.pg.query(parts_q, [this.vendors.map(x => x.vendor_id)])
  parts = parts.rows

  parts = parts.map(p => {
    if (p.side !== 'na') return { ...p, title: p.title.slice(0, -3) }
    else return p
  })

  parts = Object.values(groupby(parts, x => x.title)).filter(x => x.length === 2)

  parts = parts.map(x => x.map(y => y.id))

  if (dev) parts = sample(parts)

  this.total = parts.length

  return parts
}

function Logger (total) {
  let count = 0

  const log = part_id => _message => {
    count += 1

    const percent =
    x => (x*100).toFixed(2)

    const message = [
      `[${count}/${total}]`,
      `[${percent(count/total)}%]`,
      `[${part_id}]`,
      `${_message}\n`,
    ]

    console.log(message.join(' '))
  }

  return log
}

Client.prototype.postSingle = async function (parts_ids) {
  // init the log to always have this part id
  const log = this.log(parts_ids.join(' & '))

  const { pg, account, vendors, categories, token } = this

  // CATEGORY & VENDOR

  const parts = await pg.query('select * from parts where id = any($1)', [parts_ids])
    .then(x => x.rows)

  if (!allEquals(parts.map(x => x.vendor))) {
    log('parts should be from the same vendor')
    return
  }

  const partsVendor = parts.map(x => x.vendor)[0]

  const vendor = vendors.filter(x => x.vendor_id === partsVendor)[0]

  if (!allEquals(parts.map(x => x.category))) {
    log('parts should be of the same category')
    return
  }

  const category = categories
    .filter(x => x.vendor === vendor.vendor_name)
    .filter(x => x.category.toUpperCase() === parts[0].category.toUpperCase())
    [0]

  if (!category) {
    log('not in categories')
    return
  }

  // if (category.ignore === '1') {
  //   log(`ignore category: ${category.category}`)
  //   return
  // }

  // CARS

  const cars_q = 'select * from cars where part_id = $1'

  let cars_acc = {}
  await serial(parts.map(x => x.id).map(id => async () => {
    let cars
    cars = await pg.query(cars_q, [id])
    cars = cars.rows
    cars = cars.filter(x => x.yearlast !== undefined)
    cars = cars.map(({ part_id, ...rest }) => rest)
    cars = cars.map(({ vendor, ...rest }) => rest)

    cars_acc[id] = cars
  }))

  // set cars to the cars of the first part for the time being
  // const cars = cars_acc
  const cars = cars_acc[parts[0].id]

  // POSTS

  const posts_q = 'select * from posts where part_id = any ($1) and account = $2'

  const idPermutations = permute(parts.map(x => x.id)).map(x => x.join(' & '))

  let posts
  posts = await pg.query(posts_q, [idPermutations, account.id])
  posts = posts.rows
  posts = posts.map(part => {
    const { premium, make, model } = part
    const year_init = parseInt(part.year_init)
    const year_last = parseInt(part.year_last)
    return { premium, make, model, year_init, year_last }
  })

  // SHIPPING

  const shipping_type = category.shipping_type

  let shipping_price
  let me

  if (cache[category.category_meli]) {
    shipping_price = parseInt(cache[category.category_meli]),
    me = true
  }
  else if (shipping_type === 'ME') {
    shipping_price = await get_shipping_price(category.category_meli)
    me = true
    cache[category.category_meli] = parseInt(shipping_price)
  }
  else {
    shipping_price = parseInt(shipping_type.split(':')[1]),
    me = false
  }

  // // SYNC

  const rn = Math.random() * 5

  let posted = []

  let missing = get_missing(cars, posts)
  // let missing = get_missing(cars, [])

  if (missing.length === 0) {
    log('all already posted')
    return
  }

  let post_error = null

  missing = missing.map(m => async () => {
    const link = `${host}/items?access_token=${token}`

    let side
    let pair

    if (parts.length === 1) {
      side = parts[0].side
      pair = false
    }
    else if (parts.length === 2 && (parts[0].side === parts[1].side)) {
      side = 'na'
      pair = false
    }
    else if (parts.length === 2 && (parts[0].side !== parts[1].side)) {
      side = 'na'
      pair = true
    }

    const title = gen_title({
      category: category.title, 
      side,
      make: m.make, 
      model: m.model,
      yearinit: m.yearinit,
      yearlast: m.yearlast,
      pair,
    })

    const description = gen_desc({
      business: account.title, 
      make: m.make, 
      model: m.model, 
      category: category.category, 
      yearinit: m.yearinit, 
      yearlast: m.yearlast, 
      side: side, 
      id: parts.map(x => x.id).join(' & '),
      title: parts.map(x => x.title).join(' & '), 
      cars: cars,
      vendor_id: vendor.vendor_id,
      random: rn
    })

    const price = calc_price({
      vendorPrice: parts.map(x => Number(x.price)).reduce((a, b) => a + b, 0),
      vendorDiscount: Number(vendor.discount),
      tax: 0.16,
      shipping: shipping_price,
      // commision : m.premium ? 0.175 : 0.13,
      shippingDiscount : Number(account.less_than_550),
      // more_than_550 : Number(account.more_than_550),
      expectedProfit: Number(vendor.expected_profit),
      isPremium: m.premium,
      me,
    })

    let inventory
    inventory = min(parts.map(x => x.inventory))
    if (inventory < Number(vendor.min_inventory)) inventory = 1

    const body = {
      title,
      description: { plain_text: description },
      price,
      available_quantity: inventory,
      category_id: category.category_meli,
      // the image of the first part for now
      pictures: [ { source: parts[0].image } ],
      listing_type_id: m.premium ? 'gold_pro': 'gold_special',
      shipping: me ? shipping_me : shipping,
      currency_id: 'MXN',
      condition: 'new',
    }

    const values = [
      title,
      category.category_meli,
      inventory,
      price,
      m.premium,
      'active',
      '',
      description,
      account.id,
      parts.map(x => x.id).join(' & '),
      m.make,
      m.model,
      m.yearinit,
      m.yearlast
    ]

    const new_post_q = `insert into posts values (${range(1, 16).map(i => '$' + i)})`

    if (!dev) {
      try {
        let post_id
        post_id = await post(link, body)
        post_id = post_id.data.id
  
        await pg.query(new_post_q, [post_id].concat(values))
  
        posted.push({ id: post_id, title, premium: m.premium  })
  
      } catch (e) {
        // if (e.response.data.cause[0].code === 'item.listing_type_id.unavailable') {
        if (e.response.data.message === 'daily_quota.reached') {
          post_error = 'daily quota reached, try again in 24 hours'
        }
        else if (e.response.data.error === 'validation_error') {
          post_error = 'Validation error'
          console.log(e.response.data)
        }
        else {
          post_error = 'Unknown error'
          console.log('error posting:')
          console.log(e.response.data)
        }
      }
    }
    else {
      posted.push({ id: '#####', title, premium: m.premium  })

      console.log('====')
      console.log(`link: ${link}`)
      console.log(`body: ${JSON.stringify(body)}`)
      console.log('====')
      console.log(`query: ${new_post_q}`)
      console.log(`params: ${values}`)
    }
  })

  await parallel(missing, 1)

  if (posted.length === 0) {
    log(post_error)
    return
  }

  posted = posted.map(
    ({ id, title, premium }) =>
    `${premium ? 'premium' : 'not premium' }, ${id}, ${title}`
  )
  posted = posted.join('; ')
  log(posted)
}

Client.prototype.post = async function (parts) {
  // make a new logger everytime we start processing
  const logger = new Logger(parts.length)
  this.log = new Logger(parts.length)

  await parallel(parts.map(p => () => this.postSingle(p).catch(e => console.log(e))), 15)
}

;
(async () => {
  const pg = new _pg()
  await pg.connect()

  const token = await get_token({}).promise()

  let account_id
  account_id = await get(`${host}/users/me?access_token=${token}`)
  account_id = account_id.data.id
  
  let account_vendors
  account_vendors = await parse_csv(read_file('data/vendors_accounts.csv', 'utf-8'))
  
  let account
  account = await parse_csv(read_file('data/accounts.csv', 'utf-8'))
  account = account.filter(x => x.id === account_id.toString())[0]
  account.less_than_550 = parseFloat(account.less_than_550)
  account.more_than_550 = parseFloat(account.more_than_550)

  let vendors
  vendors = await parse_csv(read_file('data/vendors.csv', 'utf-8'))
  vendors = vendors.filter(v => {
    const accounts = account_vendors
      .filter(({ account_id, vendor_id }) => vendor_id === v.vendor_id)
      .map(x => x.account_id)

    return accounts.indexOf(account_id.toString()) !== -1
  })

  const categories = await parse_csv(read_file('data/categories.csv', 'utf-8'))

  const poster = new Client(pg)
  await poster.connect()

  cmd
    .option('-c, --category [v]', 'choose category')
    .parse(process.argv)

  let cats

  const allCats = await pg.query('select distinct category from parts')
    .then(x => x.rows.map(xx => xx.category))

  if (cmd.category && allCats.includes(cmd.category)) {
    cats = [cmd.category]
  }
  else if (cmd.category && !allCats.includes(cmd.category)) {
    throw new Error(`the category "${cmd.category}" doesnt exists`)
  }
  else {
    cats = allCats
  }

  const parts_q = `
    select * from parts 
    where vendor = any ($1)
    and category = any ($2)
  `
  let parts
  parts = await pg.query(parts_q, [vendors.map(x => x.vendor_id), cats])
    .then(x => x.rows.map(xx => [xx.id]))
    // .then(x => x.rows)
  // if (dev) parts = sample(parts, 10)

  // this.total = parts.length

  // const parts = await poster.getAllParts()
  // const pairs = await poster.getAllPairs()

  total = parts.length

  // await poster.post(parts.concat(pairs))
  await poster.post(parts)
})().catch(console.log)
