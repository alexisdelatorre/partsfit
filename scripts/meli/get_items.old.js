const auth = require('meli-auth')
const { get } = require('axios')
const sample = require('lodash.samplesize')
const parallel = require('../../utils/parallel')
const { Client: _pg } = require('pg')
const range = require('lodash.range')

const parse_desc = require('../../utils/parse-description')

const dev = process.env.NODE_ENV !== "production"

const host = 'https://api.mercadolibre.com'

const getItems = async (token, user, _scroll) => {
  let link
  link = [
    host,
    '/users',
    `/${user}`,
    '/items',
    '/search',
    '?search_type=scan',
    `&access_token=${token}`,
  ]
  link = link.join('')
  if (_scroll) link += `&scroll_id=${_scroll}`

  let search
  search = await get(link)
  search = search.data

  const items = search.results
  const scroll = search.scroll_id
  return { items, scroll }
}

const run = env => async post_id => {
  const log = _message => {
    count += 1

    const percent =
    x => (x*100).toFixed(2)

    const message = [
      `[${count}/${total}]`,
      `[${percent(count/total)}%]`,
      `[${post_id}]`,
      `${_message}\n`,
    ]

    console.log(message.join(' '))
  }

  const { token, user, pg } = env

  let exists
  exists = await pg.query('select * from posts where id = $1', [post_id])
  exists = exists.rowCount > 0
  
  if (exists) {
    log('post already exists')
    return
  }

  const itemLink = `${host}/items/${post_id}`
  const descLink = `${host}/items/${post_id}/description`

  let post
  try {
    post = await get(itemLink)
    post = post.data
  } catch (error) {
    log('error getting post')
    return
  }

  let description
  try {
    description = await get(descLink)
    description = description.data
  } catch (error) {
    log('error getting description')
    return
  }

  const id = post.id
  const title = post.title
  const category = post.category_id
  // inventory should come from available_quantity but for some
  // reason it comes from initial_quantity
  const inventory = post.initial_quantity
  const price = post.price
  const premium = post.listing_type_id === 'gold_pro'
  const status = post.status

  const description_html = description.text
  const description_text =  description.plain_text

  const parsed_desc = parse_desc(description_text)

  const values = [
    id,
    title,
    category,
    inventory,
    price,
    premium,
    status,
    description_html,
    description_text,
    user,
    parsed_desc.part_id,
    parsed_desc.make,
    parsed_desc.model,
    parsed_desc.years ? parsed_desc.years.split('-')[0] : undefined,
    parsed_desc.years ? parsed_desc.years.split('-')[1] : undefined,
  ]

  const query = `insert into posts values (${range(1, 16).map(x => '$' + x)})`
  
  if (!dev) {
    await pg.query(query, values)
  }
  else {
    console.log(query, values)
  }

  log(post.title)
}

let total
let count = 0;

(async () => {
  const token = await auth({}).promise()

  let user
  user = await get(`${host}/users/me?access_token=${token}`)
  user = user.data.id

  const pg = new _pg()
  await pg.connect()

  const create_q = `
    create table if not exists posts (
      id text,
      title text,
      category text,
      inventory int,
      price numeric,
      premium boolean,
      status text,
      description_html text,
      description_text text,
      account text,
      part_id text,
      make text,
      model text,
      year_init int,
      year_last int
    )
  `

  const delete_q = `delete from posts where account = $1`  

  const should_clean = true
  
  if (!dev) {
    await pg.query(create_q).catch(() => {})
    if (should_clean) {
      await pg.query(delete_q, [user])
    }
  }
  else {
    if (should_clean) {
      console.log(delete_q)
    }
    console.log(create_q)
  }

  const totalLink = `${host}/users/${user}/items/search?access_token=${token}`
  total = await get(totalLink)
  total = total.data.paging.total

  const env = { token, user, pg }
  
  // recursive item search
  // http://developers.mercadolibre.com/news/new-mode-search-search-resource/
  let count = 0
  let retries = 0
  const rec = async _scroll => {
    if (dev) return

    let items
    let scroll

    try {
      const x = await getItems(token, user, _scroll)
      items = x.items
      scroll = x.scroll
      retries = 0
    } catch (e) {
      count += 1
      await rec(scroll)
    }

    if (dev) items = sample(items)
  
    if (items.lenght === 0) return

    await Promise.all(items.map(run(env)))
  
    count += 1
    await rec(scroll)
  }
  await rec(undefined)

  if (dev) {
    await run(env)('MLM637446690')
  }
})()
