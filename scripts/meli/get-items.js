const Postgres = require('pg')
const Meli = require('../../utils/meli')
const Logger = require('../../utils/logger')
const sample = require('lodash.samplesize')
const range = require('lodash.range')
const parseDesc = require('../../utils/parse-description')
const serial = require('promise-serial')

const dev = process.env.NODE_ENV !== 'production'

;
(async function () {
    const pg = new Postgres.Client({ database: 'partsfit' })
    await pg.connect()

    if (!dev) pg.query('delete from posts')

    const meli = new Meli.Client()
    await meli.authenticate()

    const total = await meli.getSellerItems()
        .then(x => x.total)
    
    const logger = new Logger.Client(total)

    let count = 0
    let limit
    if (Math.floor(total / 50) > 14) limit = sample(range(1, 15))[0]
    else limit = Math.floor(total / 50)

    let scroll

    while (true) {
        let items

        await meli.getSellerItems(scroll)
            .then(x => {
                items = x.items
                scroll = x.scroll
            })

        if (dev) items = sample(items)

        if (items.length === 0) break

        if (dev && count < limit) {
            count++
            continue
        }

        await serial(items.map(itemId => async () => {
            const log = logger.log([itemId])

            let item
            try {
                item = await meli.getItem(itemId)
            } catch (error) {
                log('error')
                console.log(error)
            }

            const parsed_desc = parseDesc(item.description)

            const user_id = await meli.getUser().then(x => x.id)

            const values = [
                item.id,
                item.title,
                item.category,
                item.inventory,
                item.price,
                item.premium,
                item.status,
                '',
                item.description,
                user_id,
                parsed_desc.part_id,
                parsed_desc.make,
                parsed_desc.model,
                parsed_desc.years ? parsed_desc.years.split('-')[0] : undefined,
                parsed_desc.years ? parsed_desc.years.split('-')[1] : undefined,
            ]

            const query = `insert into posts values (${range(1, 16).map(x => '$' + x)})`

            if (!dev) {
                await pg.query(query, values)
            }
            else {
                console.log(query, values)
            }

            log(item.title)
        }), { parallelize: 15 })

        count++
        if (count > Math.floor(total / 50)) break
        if (dev) break
    }
    
})().catch(console.log)