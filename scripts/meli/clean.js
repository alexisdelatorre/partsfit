const Postgres = require('pg').Client
const auth = require('meli-auth')
const get = require('axios').get
const put = require('axios').put
const getMissing = require('../../utils/missing')
const sample = require('lodash.samplesize')
const Logger = require('../../utils/logger').Client
const Meli = require('../../utils/meli')
const serial = require('promise-serial')

const dev = process.env.NODE_ENV !== 'production'

;
(async function () {
  const pg = new Postgres({ database: 'postgres' })
  await pg.connect()

  const token = await auth({}).promise()
  const meli = new Meli({ token })

  let underReview
  underReview = await pg.query(
    'select * from posts where status = $1',
    ['under_review']
  )
    .then(x => x.rows)
  if (dev) underReview = sample(underReview)

  const logger = new Logger(underReview.length)

  await serial(underReview.map(post => async () => {
    // const post = underReview[i]

    const log = logger.log([post.id])
    
    try {
        await meli.delete(post.id)
        await pg.query('delete from posts where id = $1', [post.id])
        log('deleted')
    } catch (error) {
      if (error.response.data.status === 403) {
        log('error: forbidden')
      }
      else {
        log('there was an error')
        console.log(error.response.data)
      }
    }
  }), { parallelize: 20 })
})().catch(console.log())

// ;
// (async function () {
//   const pg = new Postgres({ database: 'postgres' })
//   await pg.connect()

//   const token = await auth({}).promise()
//   const meli = new Meli({ token })

//   const userId = await meli.getUser().then(x => x.id)

//   let parts
//   parts = await pg.query('select * from parts')
//     .then(x => x.rows)
//   if (dev) parts = parts.slice(11820,11821)
//   // if (dev) parts = sample(parts, 25)

//   for (let i = 0; i < parts.length; i++) {
//     const part = parts[i]
    
//     const cars = await pg.query('select * from cars where part_id = $1', [part.id])
//       .then(x => x.rows)
    
//     let expectedPosts = []
//     for (let i = 0; i < [true, false].length; i++) {
//       const premium = [true, false][i]
      
//       for (let i = 0; i < cars.length; i++) {
//         const car = cars[i]

//         const hash = [
//           `model:${car.model}`,
//           `premium:${premium}`
//         ].join(';')

//         expectedPosts.push(hash)
//       }
//     }

//     const posts = await pg.query('select * from posts where part_id = $1', [part.id])
//       .then(x => x.rows)

//     let hashedPosts = []
//     for (let i = 0; i < posts.length; i++) {
//       const post = posts[i]
      
//       const hash = [
//         `model:${post.model}`,
//         `premium:${post.premium}`
//       ].join(';')

//       hashedPosts.push(hash)
//     }

//     let result = {}

//     for (let i = 0; i < expectedPosts.length; i++) {
//       const expected = expectedPosts[i]
//       result[expected] = -1
//     }

//     for (let i = 0; i < hashedPosts.length; i++) {
//       const hashed = hashedPosts[i]
//       if (!result[hashed]) result[hashed] = 1
//       else result[hashed]++
//     }

//     console.log(result)

//     for (let i = 0; i < Object.keys(result).length; i++) {
//       const hash = Object.keys(result)[i]

//       if (result[hash] < 1) continue

      
//     }
//   }
// })()